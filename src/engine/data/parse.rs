use html2text::parse;
use html2text::render::text_renderer::{TaggedLine, TextDecorator};

use regex::Regex;

struct JustTextPlease {}

const NORWEGIAN_STOPWORDS: [&str; 116] = [
    "å", "alle", "andre", "arbeid", "av", "begge", "bort", "bra", "bruke",
    "da", "denne", "der", "deres", "det", "din", "disse", "du", "eller", "en",
    "ene", "eneste", "enhver", "enn", "er", "et", "få", "folk", "for", "fordi",
    "forsøke", "fra", "før", "først", "gå", "gjorde", "god", "ha", "hadde",
    "han", "hans", "hennes", "her", "hva", "hvem", "hver", "hvilken", "hvis",
    "hvor", "hvordan", "hvorfor", "i", "ikke", "inn", "innen", "kan", "kunne",
    "lage", "lang", "lik", "like", "må", "makt", "mange", "måte", "med", "meg",
    "meget", "men", "mens", "mer", "mest", "min", "mye", "nå", "når", "navn",
    "nei", "ny", "og", "også", "om", "opp", "oss", "over", "på", "part",
    "punkt", "rett", "riktig", "så", "samme", "sant", "si", "siden", "sist",
    "skulle", "slik", "slutt", "som", "start", "stille", "tid", "til",
    "tilbake", "tilstand", "under", "ut", "uten", "var", "vår", "ved", "verdi",
    "vi", "vil", "ville", "vite",
];

impl TextDecorator for JustTextPlease {
    type Annotation = ();

    fn decorate_link_start(&mut self, _: &str) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn decorate_link_end(&mut self) -> String {
        return "".to_string();
    }

    fn decorate_em_start(&mut self) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn decorate_em_end(&mut self) -> String {
        return "".to_string();
    }

    fn decorate_strong_start(&mut self) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn decorate_strong_end(&mut self) -> String {
        return "".to_string();
    }

    fn decorate_strikeout_start(&mut self) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn decorate_strikeout_end(&mut self) -> String {
        return "".to_string();
    }

    fn decorate_code_start(&mut self) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn decorate_code_end(&mut self) -> String {
        return "".to_string();
    }

    fn decorate_preformat_first(&mut self) -> Self::Annotation {
        ()
    }

    fn decorate_preformat_cont(&mut self) -> Self::Annotation {
        ()
    }

    fn decorate_image(&mut self, _: &str) -> (String, Self::Annotation) {
        ("".to_string(), ())
    }

    fn header_prefix(&mut self, _: usize) -> String {
        return "".to_string();
    }

    fn quote_prefix(&mut self) -> String {
        return "".to_string();
    }

    fn unordered_item_prefix(&mut self) -> String {
        return "".to_string();
    }

    fn ordered_item_prefix(&mut self, _: i64) -> String {
        return "".to_string();
    }

    fn make_subblock_decorator(&self) -> Self {
        Self {}
    }

    fn finalise(self) -> Vec<TaggedLine<()>> {
        Vec::new()
    }
}

pub fn clean_text(html: &str) -> String {
    let rt = parse(html.as_bytes());

    let decoder: JustTextPlease = JustTextPlease {};
    let doc = rt.render(usize::MAX, decoder).into_string().to_lowercase();

    let re = match Regex::new(r#"<[^>]*>|[:#/\\.;?!]+|\n+|[^a-zA-Zæøå]+"#) {
        Ok(r) => r,
        Err(_) => return "".to_string(),
    };

    return re.replace_all(&doc, " ").to_string();
}

/// Remove all HTML related bloat, newlines, punctuation
pub fn to_text(doc: &str) -> String {
    let stopwords = NORWEGIAN_STOPWORDS
        .iter()
        .map(|word| format!(r"\b{}\b", word))
        .collect::<Vec<_>>()
        .join("|");

    let clean_re = match Regex::new(&format!(r#"{}"#, stopwords)) {
        Ok(r) => r,
        Err(_) => return "".to_string(),
    };
    let clen = clean_re.replace_all(&doc, " ");
    let cleaned = clen.trim();

    let spaces_re = match Regex::new(r"\s+") {
        Ok(r) => r,
        Err(_) => return cleaned.to_string(),
    };

    return spaces_re.replace_all(cleaned, " ").trim().to_string();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_text_with_empty_html() {
        let html = "";
        let expected = "";
        assert_eq!(to_text(html), expected);
    }

    #[test]
    fn test_to_text_with_valid_html() {
        let html = "<html><body><p>This is a test.</p></body></html>";
        let expected = "this is a test";
        assert_eq!(to_text(html), expected);
    }

    #[test]
    fn test_to_text_with_html_containing_stopwords() {
        let html =
			"<html><body><p>This is a Test with å stopwords. så hva skjera?</p>\
             </body></html>";
        let expected = "this is a test with stopwords skjera";
        assert_eq!(to_text(html), expected);
    }

    #[test]
    fn test_to_text_with_iframe() {
        let html = "<iframe src=\"https://fpt snl no/ns html id=GTM-PFJQPQ\"\
             height=\"0\" width=\"0\" style=\"display:none visibility:hidden\">\
             </iframe>";
        let expected = "";
        assert_eq!(to_text(html), expected);
    }
}
