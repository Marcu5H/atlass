use std::fmt;
use std::fs::{create_dir, File, OpenOptions};
use std::io::BufReader;
use std::io::{BufRead, Write};
use std::path::PathBuf;

use url::Url;

use serde_json::{json, to_string_pretty};

use tokenizers::tokenizer::Tokenizer;

use sha2::{Digest, Sha256};

use config;

use stem::Stem;

mod parse;

pub enum DataError {
    DomainCrawled,
    DomainListEmpty,
    FailedToCreateDir,
    FailedToParseUrl,
    FailedToReadFromFile,
    FailedToWriteFile,
    SiteExists,
}

impl fmt::Display for DataError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DataError::DomainCrawled => {
                write!(f, "Domain already crawled")
            }
            DataError::DomainListEmpty => {
                write!(f, "Domain list is empty")
            }
            DataError::FailedToCreateDir => {
                write!(f, "Failed to create directory")
            }
            DataError::FailedToParseUrl => {
                write!(f, "Failed to parse URL")
            }
            DataError::FailedToReadFromFile => {
                write!(f, "Failed to read from file")
            }
            DataError::FailedToWriteFile => {
                write!(f, "Failed to write to file")
            }
            DataError::SiteExists => {
                write!(f, "Site already exists")
            }
        }
    }
}

pub struct SiteContext {
    path: PathBuf,
}

pub struct Data {
    data_path: String,
    domains: BufReader<File>,
    tokenizer: Tokenizer,
    stemmer: Stem,
}

impl Data {
    pub fn new(conf: &config::Engine) -> Self {
        let domains_file = match OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&conf.domains_path)
        {
            Ok(f) => f,
            Err(e) => panic!("Failed to open domains_file: {}", e),
        };

        let tok = match Tokenizer::from_file(PathBuf::from(&conf.tokenizer)) {
            Ok(t) => t,
            Err(e) => panic!("Failed to load tokenizer from file: {}", e),
        };

        return Self {
            data_path: String::from(&conf.data_path),
            domains: BufReader::new(domains_file),
            tokenizer: tok,
            stemmer: Stem::new(),
        };
    }

    fn get_hash(input: &str) -> String {
        let mut hasher = Sha256::new();
        hasher.update(input.as_bytes());
        let result = hasher.finalize();
        return hex::encode(result);
    }

    fn site_exists(&mut self, domain_hash: &str) -> (bool, PathBuf) {
        let mut site_path = PathBuf::from(&self.data_path);
        site_path.push(domain_hash);
        return (site_path.exists(), site_path);
    }

    pub fn get_domain(&mut self) -> Result<Url, DataError> {
        let mut line = String::new();
        if self.domains.read_line(&mut line).is_err() {
            return Err(DataError::FailedToReadFromFile);
        }

        if line.is_empty() {
            return Err(DataError::DomainListEmpty);
        }

        let domain = match Url::parse(&line.trim()) {
            Ok(url) => url,
            Err(_) => {
                return Err(DataError::FailedToParseUrl);
            }
        };

        let domain_hash = Self::get_hash(&domain.as_str());
        let (exists, _) = self.site_exists(&domain_hash);
        if exists {
            return Err(DataError::DomainCrawled);
        }

        return Ok(domain);
    }

    pub fn url_is_crawled(&self, ctx: &SiteContext, url: &Url) -> bool {
        let url_hash = Self::get_hash(&url.as_str());
        let mut fs_path = PathBuf::from(&ctx.path);
        fs_path.push(url_hash);
        return fs_path.exists();
    }

    pub fn init_site(
        &mut self,
        domain: &Url,
    ) -> Result<SiteContext, DataError> {
        let domain_hash = Self::get_hash(domain.as_str());
        let (exists, site_path) = self.site_exists(&domain_hash);
        if exists {
            return Err(DataError::DomainCrawled);
        }

        match create_dir(&site_path) {
            Ok(_) => return Ok(SiteContext { path: site_path }),
            Err(_) => return Err(DataError::FailedToCreateDir),
        }
    }

    pub fn save_page(
        &mut self,
        ctx: &SiteContext,
        url: &Url,
        site: &str,
    ) -> Option<DataError> {
        let site_hash = Self::get_hash(&url.as_str());
        let mut site_path = PathBuf::from(&ctx.path);
        site_path.push(&site_hash);
        if site_path.exists() {
            return Some(DataError::SiteExists);
        }

        if std::fs::create_dir_all(&site_path).is_err() {
            return Some(DataError::FailedToCreateDir);
        }

        let mut url_path = PathBuf::from(&site_path);
        url_path.push("url");
        let mut url_file =
            match OpenOptions::new().write(true).create(true).open(url_path) {
                Ok(f) => f,
                Err(_) => return Some(DataError::FailedToWriteFile),
            };
        if url_file.write_all(url.as_str().as_bytes()).is_err() {
            return Some(DataError::FailedToWriteFile);
        }

        let doc_clean_html = parse::clean_text(site);

        let mut doc_path = PathBuf::from(&site_path);
        doc_path.push("doc");
        let mut doc_file =
            match OpenOptions::new().write(true).create(true).open(doc_path) {
                Ok(f) => f,
                Err(_) => return Some(DataError::FailedToWriteFile),
            };

        if doc_file.write_all(doc_clean_html.as_bytes()).is_err() {
            return Some(DataError::FailedToWriteFile);
        }

        let doc_data = parse::to_text(&doc_clean_html);

        let mut tokenized_doc_path = PathBuf::from(&site_path);
        tokenized_doc_path.push("tokenized_doc.json");
        let mut tokenized_doc_file = match OpenOptions::new()
            .write(true)
            .create(true)
            .open(tokenized_doc_path)
        {
            Ok(f) => f,
            Err(_) => return Some(DataError::FailedToWriteFile),
        };

        let stemmed_doc_data = self.stemmer.stem(&doc_data);
        let encoded_doc = match self.tokenizer.encode(stemmed_doc_data, false) {
            Ok(data) => data,
            Err(_) => return Some(DataError::FailedToWriteFile), // TODO:
                                                                 // Write failed to tokenize document error value
        };

        let encoded_json = match to_string_pretty(&json!(encoded_doc.get_ids()))
        {
            Ok(j) => j,
            Err(_) => return Some(DataError::FailedToWriteFile), // TODO:
                                                                 // Write failed to encode json error value
        };

        match tokenized_doc_file.write_all(encoded_json.as_bytes()) {
            Ok(_) => return None,
            Err(_) => return Some(DataError::FailedToWriteFile),
        }
    }
}
