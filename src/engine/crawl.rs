// TODO: Exit if no internet was connected
// TODO: Write comments

use rand::{thread_rng, Rng};
use std::collections::{HashSet, VecDeque};
use std::fmt;
use std::time::{Duration, SystemTime};

use log::{error, info, warn};

use url::{Position, Url};

use select::document::Document;
use select::predicate::Name;

use crate::data;
use crate::data::{DataError, SiteContext};
use crate::scrape;
use config;

const ACCEPTED_LANGUAGES: [&str; 10] = [
    "no", "nb", "no-NB", "no-nb", "NO", "NB", "NO-nb", "NB-no", "nb-NO",
    "nb-no",
];

pub enum CrawlError {
    Unknown,
    SiteExists,
    FailedToGetBaseUrl,
    FailedToSendRequest,
    FailedToInitSite,
}

impl fmt::Display for CrawlError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CrawlError::Unknown => {
                return write!(f, "Unknown error");
            }
            CrawlError::SiteExists => {
                return write!(f, "Site exists");
            }
            CrawlError::FailedToGetBaseUrl => {
                return write!(f, "Failed to get base url");
            }
            CrawlError::FailedToSendRequest => {
                return write!(f, "Failed to send request");
            }
            CrawlError::FailedToInitSite => {
                return write!(f, "Failed to init site");
            }
        }
    }
}

fn get_document_lang(doc: &str) -> Option<String> {
    let document = Document::from(doc);
    let html_element = document.find(Name("html")).next()?;
    return Some(html_element.attr("lang").unwrap_or("").to_owned());
}

fn is_valid_lang(lang: &str) -> bool {
    return ACCEPTED_LANGUAGES.contains(&lang);
}

pub struct Crawler {
    max_depth: u16,
    dat: data::Data,
}

impl Crawler {
    pub fn new(conf: &config::Engine) -> Self {
        let dat = data::Data::new(conf);

        return Self {
            max_depth: conf.depth,
            dat,
        };
    }

    fn crawl_from_queue(
        &mut self,
        queue: &mut VecDeque<(Url, u16)>,
        ctx: &data::SiteContext,
        base_url: &Url,
    ) -> Option<CrawlError> {
        while let Some(job) = queue.pop_front() {
            if job.1 == 1 {
                continue;
            } else if self.dat.url_is_crawled(&ctx, &job.0) {
                continue;
            }

            let start_time = SystemTime::now();
            let delay_duration =
                Duration::from_millis(thread_rng().gen_range(1000..3500));

            match scrape::do_request(&job.0) {
                Ok(response) => {
                    if let Some(lang) = get_document_lang(&response) {
                        // Save only pages with the language set to norwegian
                        if is_valid_lang(&lang) {
                            if let Some(err) =
                                self.dat.save_page(ctx, &job.0, &response)
                            {
                                error!("{}", err);
                            }

                            // TODO: Make function "update_queue"
                            let scraped_urls: HashSet<Url> =
                                scrape::get_urls(base_url, &response);
                            if scraped_urls.len() > 0 {
                                info!(
                                    "Found {} urls on {}",
                                    scraped_urls.len(),
                                    job.0.as_str()
                                );
                                queue.extend(
                                    scraped_urls
                                        .iter()
                                        .map(|url| (url.clone(), job.1 - 1)),
                                );
                            }
                        } else {
                            warn!("Page was not norwegian ({})", lang);
                        }
                    }
                }
                Err(err) => {
                    error!("{}", err);
                }
            }

            let elapsed_time = start_time.elapsed().unwrap();
            let mut delay = delay_duration;
            if elapsed_time < delay_duration {
                delay = delay_duration - elapsed_time;
                info!("Sleeping for {} ms", delay.as_millis());
                std::thread::sleep(delay);
            }
        }
        return None;
    }

    fn crawl_site(&mut self, url: &Url) -> Option<CrawlError> {
        let response = scrape::do_request(&url);
        match response {
            Ok(r) => {
                let base_url = match Url::parse(&url[..Position::BeforePath]) {
                    Ok(u) => u,
                    //Err(e) => {
                    Err(_) => {
                        return Some(CrawlError::FailedToGetBaseUrl);
                    }
                };

                let ctx: SiteContext = match self.dat.init_site(&base_url) {
                    Ok(c) => {
                        info!(
                            "Created data dir for site ({})",
                            base_url.as_str()
                        );
                        c
                    }
                    Err(err) => match err {
                        DataError::SiteExists => {
                            return Some(CrawlError::SiteExists);
                        }
                        _ => {
                            return Some(CrawlError::FailedToInitSite);
                        }
                    },
                };

                let urls: HashSet<Url> = scrape::get_urls(&base_url, &r);
                info!(
                    "Found {} urls in root ({})",
                    urls.len(),
                    base_url.as_str()
                );

                let mut queue: VecDeque<(Url, u16)> = urls
                    .iter()
                    .map(|url| (url.clone(), self.max_depth))
                    .collect();

                self.crawl_from_queue(&mut queue, &ctx, &base_url);
            }
            //Err(err) => {
            Err(_) => {
                return Some(CrawlError::FailedToSendRequest);
            }
        }

        return None;
    }

    pub fn start_crawling(&mut self) -> Option<CrawlError> {
        loop {
            match self.dat.get_domain() {
                Ok(url) => {
                    if let Some(err) = self.crawl_site(&url) {
                        error!("{}", err);
                    }
                }
                Err(err) => match err {
                    DataError::DomainListEmpty => {
                        return None;
                    }
                    DataError::DomainCrawled => {
                        warn!("{}", err);
                    }
                    _ => {
                        return Some(CrawlError::Unknown);
                    }
                },
            }
        }
    }
}

// Tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_no() {
        let doc = "<html lang = \"no\"><head><meta></meta></head></html>";
        let expected = "no";

        assert_eq!(get_document_lang(doc).unwrap(), expected);
    }

    #[test]
    fn should_return_nb() {
        let doc = "<html lang = \"nb\"><head><meta></meta></head></html>";
        let expected = "nb";

        assert_eq!(get_document_lang(doc).unwrap(), expected);
    }

    #[test]
    fn should_return_empty() {
        let doc = "<html><head><meta></meta></head></html>";
        let expected = "";

        assert_eq!(get_document_lang(doc).unwrap(), expected);
    }
}
