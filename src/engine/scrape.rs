// TODO: Retrurn errors if ratelimited, no internett, etc.

use rand::Rng;
use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use select::document::Document;
use select::predicate::Name;
use std::collections::HashSet;
use std::time::Duration;
use url::Url;

const USER_AGENTS: [&str; 1233] = [
	"Mozilla/5.0 (X11;U;Linux i686;en-US;rv:1.8.1) Gecko/2006101022 Firefox/2.0\
     ",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.8.1) Gecko/20061228 Firefox/\
     2.0",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.8.1) Gecko/20061024 Firefox/\
     2.0",
	"Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.8.1) Gecko/20061211 Firefox/\
     2.0",
	"Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.8.1) Gecko/20061024 Firefox/\
     2.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1) Gecko/20061202 Firefox\
     /2.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1) Gecko/20061128 Firefox\
     /2.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1) Gecko/20061122 Firefox\
     /2.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1) Gecko/20061023 SUSE/2.\
     0-37 Firefox/2.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.1) Gecko/20060601 Firefox\
     /2.0 (Ubuntu-edgy)",
    "Mozilla/5.0 (X11; U; Linux x86-64; en-US; rv:1.8.1) Gecko/20061010 Firefox\
     /2.0",
	"Mozilla/5.0 (X11; U; Linux i686; zh-TW; rv:1.8.1) Gecko/20061010 Firefox/2\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; tr-TR; rv:1.8.1) Gecko/20061023 SUSE/2.0-\
     30 Firefox/2.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061127 Firefox/2.0 \
     (Gentoo Linux)",
    "Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061127 Firefox/2.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061024 Firefox/2.0 \
     (Swiftfox)",
    "Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061010 Firefox/2.0 \
     Ubuntu",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061010 Firefox/2.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1) Gecko/20061003 Firefox/2.0 \
     Ubuntu",
	"Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.8.1) Gecko/20061010 Firefox/2\
     .0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; ; rv:1.8.0.7) Gecko/20060917 Fire\
     fox/1.9.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; ; rv:1.8.0.10) Gecko/20070216 Fir\
     efox/1.9.0.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9a1) Gecko/20060112 Firefox\
     /1.6a1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a1) Gecko/20060217 Firefox/1\
     .6a1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a1) Gecko/20060117 Firefox/1\
     .6a1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a1) Gecko/20051215 Firefox/1\
     .6a1 (Swiftfox)",
    "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.9a1) Gecko/20060127 \
     Firefox/1.6a1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2 x64; en-US; rv:1.9a1) Gecko/200602\
     14 Firefox/1.6a1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9a1) Gecko/20060323 F\
     irefox/1.6a1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9a1) Gecko/20060121 F\
     irefox/1.6a1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9a1) Gecko/20051220 F\
     irefox/1.6a1",
	"Mozilla/5.0 (Windows NT 5.1; rv:1.9a1) Gecko/20060217 Firefox/1.6a1",
	"Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051002 Firefox/1\
     .6a1",
	"Mozilla/5.0 (X11; U; OpenBSD amd64; en-US; rv:1.8.0.9) Gecko/20070101 Fire\
     fox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.9) Gecko/20070126 Ubunt\
     u/dapper-security Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.9) Gecko/20071025 Firefox\
     /1.5.0.9 (Debian-2.0.0.9-2)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20070316 CentOS/\
     1.5.0.9-10.el5.centos Firefox/1.5.0.9 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20070126 Ubuntu/\
     dapper-security Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20070102 Ubuntu/\
     dapper-security Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20061221 Fedora/\
     1.5.0.9-1.fc5 Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20061219 Fedora/\
     1.5.0.9-1.fc6 Firefox/1.5.0.9 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20061215 Red Hat\
     /1.5.0.9-0.1.el4 Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20060911 SUSE/1.\
     5.0.9-3.2 Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.9) Gecko/20060911 SUSE/1.\
     5.0.9-0.2 Firefox/1.5.0.9",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.9) Gecko/2006121\
     9 Fedora/1.5.0.9-1.fc6 Firefox/1.5.0.9 pango-text",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.0.9) Gecko/20061206\
     Firefox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.0.9) Gecko/20061206\
     Firefox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.8.0.9) Gecko/20061206\
     Firefox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; tr; rv:1.8.0.9) Gecko/20061206 Fi\
     refox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; pt-BR; rv:1.8.0.9) Gecko/20061206\
     Firefox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.8.0.9) Gecko/20061206 Fi\
     refox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.8.0.9) Gecko/20061206 Fi\
     refox/1.5.0.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.0.9) Gecko/20061206 Fi\
     refox/1.5.0.9",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.8) Gecko/20061110 Firef\
     ox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; sv-SE; rv:1.8.0.8) Gecko/20061108 Fedora/\
     1.5.0.8-1.fc5 Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.8) Gecko/20061213 Firefox/1.\
     5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20061115 Ubuntu/\
     dapper-security Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20061110 Firefox\
     /1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20061107 Fedora/\
     1.5.0.8-1.fc6 Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20061025 Firefox\
     /1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20060911 SUSE/1.\
     5.0.8-0.2 Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.8) Gecko/20060802 Mandriv\
     a/1.5.0.8-1.1mdv2007.0 (2007.0) Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.8.0.8) Gecko/20061025 Firefox\
     /1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.8) Gecko/20061115 Ubuntu/dap\
     per-security Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.8) Gecko/20061025 Firefox/1.\
     5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.8) Gecko/20060911 SUSE/1.5.0\
     .8-0.2 Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.8) Gecko/2006102\
     5 Firefox/1.5.0.8",
	"Mozilla/5.0 (X11; U; Linux Gentoo i686; pl; rv:1.8.0.8) Gecko/20061219 Fir\
     efox/1.5.0.8",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.8.0.8) Gecko/20061210 Firef\
     ox/1.5.0.8",
	"Mozilla/5.0 (X11; U; FreeBSD amd64; en-US; rv:1.8.0.8) Gecko/20061116 Fire\
     fox/1.5.0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.0.8) Gecko/20061025\
     Firefox/1.5.0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.0.8) Gecko/20061025\
     Firefox/1.5.0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.8.0.8) Gecko/20061025 Fi\
     refox/1.5.0.8",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.8.0.7) Gecko/20060915 Firefo\
     x/1.5.0.7",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.7) Gecko/20061017 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.7) Gecko/20060920 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; NetBSD amd64; fr-FR; rv:1.8.0.7) Gecko/20061102 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.7) Gecko/20060924 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.7) Gecko/20060921 Ubunt\
     u/dapper-security Firefox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.7) Gecko/20060919 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.7) Gecko/20060911 Firef\
     ox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; sk; rv:1.8.0.7) Gecko/20060909 Firefox/1.\
     5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.8.0.7) Gecko/20060921 Ubuntu/dap\
     per-security Firefox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.7) Gecko/20060914 Firefox/1.\
     5.0.7 (Swiftfox)",
    "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.8.0.7) Gecko/20060914 Firefox\
     /1.5.0.7 (Swiftfox) Mnenhy/0.7.4.666",
	"Mozilla/5.0 (X11; U; Linux i686; ko-KR; rv:1.8.0.7) Gecko/20060913 Fedora/\
     1.5.0.7-1.fc5 Firefox/1.5.0.7 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; hu; rv:1.8.0.7) Gecko/20060911 SUSE/1.5.0\
     .7-0.1 Firefox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.7) Gecko/20060921 Ubuntu/dap\
     per-security Firefox/1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.7) Gecko/20060909 Firefox/1.\
     5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:1.8.0.7) Gecko/20060830 Firefox\
     /1.5.0.7 (Debian-1.5.dfsg+1.5.0.7-1~bpo.1)",
    "Mozilla/5.0 (X11; U; Linux i686; es-AR; rv:1.8.0.7) Gecko/20060909 Firefox\
     /1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; en-ZW; rv:1.8.0.7) Gecko/20061018 Firefox\
     /1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20061014 Firefox\
     /1.5.0.7",
	"Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.8.0.6) Gecko/20060728 Firefox\
     /1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; nl; rv:1.8.0.6) Gecko/20060728 Firefox/1.\
     5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.6) Gecko/20060728 Firefox/1.\
     5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060905 Fedora/\
     1.5.0.6-10 Firefox/1.5.0.6 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060808 Fedora/\
     1.5.0.6-2.fc5 Firefox/1.5.0.6 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060807 Firefox\
     /1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060803 Firefox\
     /1.5.0.6 (Swiftfox)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060802 Firefox\
     /1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060728 SUSE/1.\
     5.0.6-0.1 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060728 Firefox\
     /1.5.0.6 (Debian-1.5.dfsg+1.5.0.6-4)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060728 Firefox\
     /1.5.0.6 (Debian-1.5.dfsg+1.5.0.6-1)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.6) Gecko/20060728 Firefox\
     /1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.8.0.6) Gecko/20060808 Fedora/\
     1.5.0.6-2.fc5 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.6) Gecko/20060808 Fedora/1.5\
     .0.6-2.fc5 Firefox/1.5.0.6 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); zh-TW; rv:1.8.0.6) Gecko/2006072\
     8 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); nl; rv:1.8.0.6) Gecko/20060728 S\
     USE/1.5.0.6-1.2 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.6) Gecko/2006072\
     8 SUSE/1.5.0.6-1.2 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.6) Gecko/2006072\
     8 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); de; rv:1.8.0.6) Gecko/20060728 S\
     USE/1.5.0.6-1.3 Firefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); de; rv:1.8.0.6) Gecko/20060728 F\
     irefox/1.5.0.6",
	"Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.8.0.5) Gecko/20060728 Firefo\
     x/1.5.0.5",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.5) Gecko/20060819 Firef\
     ox/1.5.0.5",
	"Mozilla/5.0 (X11; U; NetBSD i386; en-US; rv:1.8.0.5) Gecko/20060818 Firefo\
     x/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.5) Gecko/20060911 Firef\
     ox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.5) Gecko/20060731 Ubunt\
     u/dapper-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; sv-SE; rv:1.8.0.5) Gecko/20060731 Ubuntu/\
     dapper-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.8.0.5) Gecko/20060731 Ubuntu/\
     dapper-security Firefox/1.5.0.5 Mnenhy/0.7.4.666",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.5) Gecko/20060731 Ubuntu/dap\
     per-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060831 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060820 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060813 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060812 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060806 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060803 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060801 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060731 Ubuntu/\
     dapper-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.5) Gecko/20060719 Firefox\
     /1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.8.0.5) Gecko/20060731 Ubuntu/\
     dapper-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.5) Gecko/20060731 Ubuntu/dap\
     per-security Firefox/1.5.0.5",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.5) Gecko/2006072\
     6 Red Hat/1.5.0.5-0.el4.1 Firefox/1.5.0.5 pango-text",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.4) Gecko/20060628 Firef\
     ox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.4) Gecko/20060608 Ubunt\
     u/dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; ru; rv:1.8.0.4) Gecko/20060508 Firefox/1.\
     5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.8.0.4) Gecko/20060608 Ubuntu/\
     dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.4) Gecko/20060614 Fedora/1.5\
     .0.4-1.2.fc5 Firefox/1.5.0.4 pango-text Mnenhy/0.7.4.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.4) Gecko/20060527 SUSE/1.5.0\
     .4-1.7 Firefox/1.5.0.4 Mnenhy/0.7.4.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.8.0.4) Gecko/20060608 Ubuntu/\
     dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; nl; rv:1.8.0.4) Gecko/20060608 Ubuntu/dap\
     per-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:1.8.0.4) Gecko/20060608 Ubuntu/\
     dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; es-AR; rv:1.8.0.4) Gecko/20060608 Ubuntu/\
     dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060716 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060711 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060704 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060629 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060614 Fedora/\
     1.5.0.4-1.2.fc5 Firefox/1.5.0.4 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060613 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060608 Ubuntu/\
     dapper-security Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060527 SUSE/1.\
     5.0.4-1.3 Firefox/1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060508 Firefox\
     /1.5.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.4) Gecko/20060406 Firefox\
     /1.5.0.4 (Debian-1.5.dfsg+1.5.0.4-1)",
    "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.3) Gecko/20060523 Ubunt\
     u/dapper Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.3) Gecko/20060522 Firef\
     ox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.8.0.3) Gecko/20060523 Ubuntu/\
     dapper Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060523 Ubuntu/\
     dapper Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060504 Fedora/\
     1.5.0.3-1.1.fc5 Firefox/1.5.0.3 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060426 Firefox\
     /1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060425 SUSE/1.\
     5.0.3-7 Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060326 Firefox\
     /1.5.0.3 (Debian-1.5.dfsg+1.5.0.3-2)",
    "Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.8.0.3) Gecko/20060426 Firefox\
     /1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.3) Gecko/20060426 Firefox/1.\
     5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.3) Gecko/20060425 SUSE/1.5.0\
     .3-7 Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); ru; rv:1.8.0.3) Gecko/20060425 S\
     USE/1.5.0.3-7 Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.3) Gecko/2006042\
     6 Firefox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.4) Gecko/20060508\
     Firefox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.3) Gecko/20060426\
     Firefox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.0.3) Gecko/20060426\
     Firefox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.8.0.3) Gecko/20060426 Fi\
     refox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; es-ES; rv:1.8.0.3) Gecko/20060426\
     Firefox/1.5.0.3",
	"Mozilla/5.0 (Windows; U; Win 9x 4.90; en-US; rv:1.8.0.3) Gecko/20060426 Fi\
     refox/1.5.0.3",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; es-ES; rv:1.8.0.3) Gecko/2\
     0060426 Firefox/1.5.0.3",
	"Mozilla/5.0 (X11; U; OpenBSD sparc64; pl-PL; rv:1.8.0.2) Gecko/20060429 Fi\
     refox/1.5.0.2",
	"Mozilla/5.0 (X11; U; OpenBSD sparc64; en-CA; rv:1.8.0.2) Gecko/20060429 Fi\
     refox/1.5.0.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; de-AT; rv:1.8.0.2) Gecko/20060422 Firef\
     ox/1.5.0.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.2) Gecko/20060419 Fedora/\
     1.5.0.2-1.2.fc5 Firefox/1.5.0.2 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.2) Gecko/20060308 Firefox\
     /1.5.0.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.2) Gecko Firefox/1.5.0.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050921 Firefox/\
     1.5.0.2 Mandriva/1.0.6-15mdk (2006.0)",
    "Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.8.0.2) Gecko/20060414 Firef\
     ox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-TW; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; sv-SE; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; pt-BR; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.8.0.2) Gecko/20060308 Fi\
     refox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.0.2) Gecko/20060308 Fi\
     refox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.8.0.2) Gecko/20060308 Fi\
     refox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.2) Gecko/20060419\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.2) Gecko/20060406\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.2) Gecko/20060309\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.2) Gecko/20060308\
     Firefox/1.5.0.2",
	"Mozilla/5.0 (X11; U; Linux i686; sv-SE; rv:1.8.0.13pre) Gecko/20071126 Ubu\
     ntu/dapper-security Firefox/1.5.0.13pre",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.13pre) Gecko/20080207 Ubu\
     ntu/dapper-security Firefox/1.5.0.13pre",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.12) Gecko/20080419 Cent\
     OS/1.5.0.12-0.15.el4.centos Firefox/1.5.0.12 pango-text",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.12) Gecko/20070718 Red \
     Hat/1.5.0.12-3.el5 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.12) Gecko/20070530 Fedo\
     ra/1.5.0.12-1.fc6 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.12) Gecko/20070508 Firefox/1\
     .5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; nl; rv:1.8.0.12) Gecko/20070601 Ubuntu/da\
     pper-security Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20071126 Fedora\
     /1.5.0.12-7.fc6 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070719 CentOS\
     /1.5.0.12-0.3.el4.centos Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070530 Fedora\
     /1.5.0.12-1.fc6 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070529 Red Ha\
     t/1.5.0.12-0.1.el4 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.8.0.12) Gecko/20070718 Fedora\
     /1.5.0.12-4.fc6 Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.12) Gecko/20070731 Ubuntu/da\
     pper-security Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.12) Gecko/20070719 CentOS/1.\
     5.0.12-3.el5.centos Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.12) Gecko/200803\
     26 CentOS/1.5.0.12-14.el5.centos Firefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.12) Gecko/200707\
     31 Ubuntu/dapper-security Firefox/1.5.0.12",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.0.12) Gecko/2007050\
     8 Firefox/1.5.0.12 (.NET CLR 3.5.30729)",
    "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.0.12) Gecko/2007050\
     8 Firefox/1.5.0.12",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8.0.12) Gecko/2007050\
     8 Firefox/1.5.0.12",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; sv-SE; rv:1.8.0.12) Gecko/2007050\
     8 Firefox/1.5.0.12",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; nl; rv:1.8.0.12) Gecko/20070508 F\
     irefox/1.5.0.12",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ko; rv:1.8.0.12) Gecko/20070508 F\
     irefox/1.5.0.12",
	"Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:1.8.0.11) Gecko/20070327 Ubuntu\
     /dapper-security Firefox/1.5.0.11",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.11) Gecko/20070312 Firefo\
     x/1.5.0.11",
	"Mozilla/5.0 (X11; U; Linux i686; de; rv:1.8.0.11) Gecko/20070327 Ubuntu/da\
     pper-security Firefox/1.5.0.11",
	"Mozilla/5.0 (X11; U; Linux i686; cs-CZ; rv:1.8.0.11) Gecko/20070327 Ubuntu\
     /dapper-security Firefox/1.5.0.11",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.11) Gecko/200703\
     12 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; nl; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; hu; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fi; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.8.0.11) Gecko/2007031\
     2 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.12) Gecko/2007050\
     8 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.11) Gecko/2007031\
     2 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; pl; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; it; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; fr; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; es-ES; rv:1.8.0.11) Gecko/2007031\
     2 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.8.0.11) Gecko/2007031\
     2 Firefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de; rv:1.8.0.11) Gecko/20070312 F\
     irefox/1.5.0.11",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.8.0.10pre) Gecko/2007\
     0207 Firefox/1.5.0.10pre",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.10pre) Gecko/2007\
     0211 Firefox/1.5.0.10pre",
	"Mozilla/5.0 (X11; U; OpenBSD ppc; en-US; rv:1.8.0.10) Gecko/20070223 Firef\
     ox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.10) Gecko/20070409 Cent\
     OS/1.5.0.10-2.el5.centos Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; zh-TW; rv:1.8.0.10) Gecko/20070508 Fedora\
     /1.5.0.10-1.fc5 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; ja; rv:1.8.0.10) Gecko/20070510 Fedora/1.\
     5.0.10-6.fc6 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.10) Gecko/20070223 Fedora/1.\
     5.0.10-1.fc5 Firefox/1.5.0.10 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070510 Fedora\
     /1.5.0.10-6.fc6 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070409 CentOS\
     /1.5.0.10-2.el5.centos Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070302 Ubuntu\
     /dapper-security Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070226 Red Ha\
     t/1.5.0.10-0.1.el4 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070226 Fedora\
     /1.5.0.10-1.fc6 Firefox/1.5.0.10 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070223 CentOS\
     /1.5.0.10-0.1.el4.centos Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070221 Red Ha\
     t/1.5.0.10-0.1.el4 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20070216 Firefo\
     x/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.10) Gecko/20060911 SUSE/1\
     .5.0.10-0.2 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-CA; rv:1.8.0.10) Gecko/20070223 Fedora\
     /1.5.0.10-1.fc5 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686; cs-CZ; rv:1.8.0.10) Gecko/20070313 Fedora\
     /1.5.0.10-5.fc6 Firefox/1.5.0.10",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.0.10) Gecko/200609\
     11 SUSE/1.5.0.10-0.2 Firefox/1.5.0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; sv-SE; rv:1.8.0.10) Gecko/2007021\
     6 Firefox/1.5.0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ja; rv:1.8.0.10) Gecko/20070216 F\
     irefox/1.5.0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.8.0.10) Gecko/20070216 F\
     irefox/1.5.0.10",
	"Mozilla/5.0 (ZX-81; U; CP/M86; en-US; rv:1.8.0.1) Gecko/20060111 Firefox/1\
     .5.0.1",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.8.0.1) Gecko/20060206 Firefo\
     x/1.5.0.1",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-GB; rv:1.8.0.1) Gecko/20060206 Firefo\
     x/1.5.0.1",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.1) Gecko/20060213 Firef\
     ox/1.5.0.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:1.8) Gecko/20051128 SUSE/1.5-\
     0.1 Firefox/1.5.0.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8.0.1) Gecko/20060313 Fedor\
     a/1.5.0.1-9 Firefox/1.5.0.1 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.8.0.1) Gecko/20060124 Firefox/1.5.0.\
     1",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.1) Gecko/20060313 Fedora/1.5\
     .0.1-9 Firefox/1.5.0.1 pango-text Mnenhy/0.7.3.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.1) Gecko/20060201 Firefox/1.\
     5.0.1 (Swiftfox) Mnenhy/0.7.3.0",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.1) Gecko/20060124 Firefox/1.\
     5.0.1 Ubuntu",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.0.1) Gecko/20060124 Firefox/1.\
     5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.8.0.1) Gecko/20060313 Fedora/\
     1.5.0.1-9 Firefox/1.5.0.1 pango-text Mnenhy/0.7.3.0",
	"Mozilla/5.0 (X11; U; Linux i686; it; rv:1.8.0.1) Gecko/20060124 Firefox/1.\
     5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8.0.1) Gecko/20060124 Firefox/1.\
     5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; es-ES; rv:1.8.0.1) Gecko/20060124 Firefox\
     /1.5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.7) Gecko/20060911 Red Hat\
     /1.5.0.7-0.1.el4 Firefox/1.5.0.1 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.1) Gecko/20060404 Firefox\
     /1.5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.1) Gecko/20060324 Ubuntu/\
     dapper Firefox/1.5.0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.1) Gecko/20060313 Fedora/\
     1.5.0.1-9 Firefox/1.5.0.1 pango-text",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.1) Gecko/20060313 Debian/\
     1.5.dfsg+1.5.0.1-4 Firefox/1.5.0.1",
	"Mozilla/5.0 (X11; Linux i686; U; en; rv:1.8.0) Gecko/20060728 Firefox/1.5.\
     0",
	"Mozilla/5.0 (Windows NT 5.2; U; de; rv:1.8.0) Gecko/20060728 Firefox/1.5.0\
     ",
	"Mozilla/5.0 (Windows NT 5.1; U; tr; rv:1.8.0) Gecko/20060728 Firefox/1.5.0\
     ",
	"Mozilla/5.0 (Windows NT 5.1; U; en; rv:1.8.0) Gecko/20060728 Firefox/1.5.0\
     ",
	"Mozilla/5.0 (Windows NT 5.1; U; de; rv:1.8.0) Gecko/20060728 Firefox/1.5.0\
     ",
	"Mozilla/5.0 (Windows 98; U; en; rv:1.8.0) Gecko/20060728 Firefox/1.5.0",
	"Mozilla/5.0 (Macintosh; PPC Mac OS X; U; en; rv:1.8.0) Gecko/20060728 Fire\
     fox/1.5.0",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.8) Gecko/20051130 Firefox/1.\
     5",
	"Mozilla/5.0 (X11; U; NetBSD i386; en-US; rv:1.8) Gecko/20060104 Firefox/1.\
     5",
	"Mozilla/5.0 (X11; U; Linux x86_64; fr; rv:1.8) Gecko/20051231 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8) Gecko/20051212 Firefox/1\
     .5",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.8) Gecko/20051201 Firefox/1\
     .5",
	"Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.8) Gecko/20051111 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8) Gecko/20051111 Firefox/1.5 Ub\
     untu",
	"Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8) Gecko/20051111 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; lt; rv:1.6) Gecko/20051114 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; lt-LT; rv:1.6) Gecko/20051114 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; it; rv:1.8) Gecko/20060113 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8) Gecko/20060110 Debian/1.5.dfs\
     g-4 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.8) Gecko/20051111 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; fr-FR; rv:1.8) Gecko/20051111 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060806 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060130 Ubuntu/1.5.\
     dfsg-4ubuntu6 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060119 Debian/1.5.\
     dfsg-4ubuntu3 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060118 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060111 Firefox/1.5\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8) Gecko/20060110 Debian/1.5.\
     dfsg-4 Firefox/1.5",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8b5) Gecko/20051008 Fedora/1.\
     5-0.5.0.beta2 Firefox/1.4.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8b5) Gecko/20051006 Firefox/1\
     .4.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.8b5) Gecko/20051006 F\
     irefox/1.4.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; tr; rv:1.8b5) Gecko/20051006 Fire\
     fox/1.4.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8b5) Gecko/20051006 Fire\
     fox/1.4.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8b5) Gecko/20051006 F\
     irefox/1.4.1",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8b5) Gecko/200\
     51006 Firefox/1.4.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8b4) Gecko/20050908 F\
     irefox/1.4",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.8b4) Gecko/20050908 F\
     irefox/1.4",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8b4) Gecko/200\
     50908 Firefox/1.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.13) Gecko/20060413 Red Hat/\
     1.0.8-1.4.1 Firefox/1.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.13) Gecko/20060411 Firefox/\
     1.0.8 SUSE/1.0.8-0.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20060410 Firefox/\
     1.0.8 Mandriva/1.0.6-16.5.20060mdk (2006.0)",
    "Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.7.13) Gecko/20060418 Fedora/1\
     .0.8-1.1.fc4 Firefox/1.0.8",
	"Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.7.13) Gecko/20060418 Firefox/\
     1.0.8 (Ubuntu package 1.0.8)",
    "Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.7.13) Gecko/20060411 Firefox/\
     1.0.8 SUSE/1.0.8-0.2",
	"Mozilla/5.0 (X11; U; Linux i686; da-DK; rv:1.7.13) Gecko/20060411 Firefox/\
     1.0.8 SUSE/1.0.8-0.2",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.7.12) Gecko/20051105 Firefo\
     x/1.0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.13) Gecko/20060410 \
     Firefox/1.0.8",
	"Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.7.13) Gecko/20060410 Firefox/1\
     .0.8",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.7.13) Gecko/20\
     060410 Firefox/1.0.8",
	"Mozilla/5.0 (X11; U; x86_64 Linux; en_US; rv:1.7.12) Gecko/20050915 Firefo\
     x/1.0.7",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.7.12) Gecko/20050927 Firefox\
     /1.0.7",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.7.12) Gecko/20050922 Firefox\
     /1.0.7",
	"Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.7.12) Gecko/20051121 Firefox\
     /1.0.7 (Nexenta package 1.0.7)",
	"Mozilla/5.0 (X11; U; Linux x86_64; fr; rv:1.7.12) Gecko/20050922 Fedora/1.\
     0.7-1.1.fc4 Firefox/1.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20060202 CentOS\
     /1.0.7-1.4.3.centos4 Firefox/1.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20051218 Firefo\
     x/1.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20051127 Firefo\
     x/1.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20051010 Firefo\
     x/1.0.7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20051010 Firefo\
     x/1.0.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.12) Gecko/20050922 Fedora\
     /1.0.7-1.1.fc4 Firefox/1.0.7",
	"Mozilla/5.0 (X11; U; Linux ppc; en-US; rv:1.7.12) Gecko/20051222 Firefox/1\
     .0.7",
	"Mozilla/5.0 (X11; U; Linux ppc; da-DK; rv:1.7.12) Gecko/20051010 Firefox/1\
     .0.7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.7.12) Gecko/20051010 Firefox/\
     1.0.7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.7.12) Gecko/20051010 Firefox/\
     1.0.7 (Ubuntu package 1.0.7)",
	"Mozilla/5.0 (X11; U; Linux i686; it-IT; rv:1.7.12) Gecko/20051010 Firefox/\
     1.0.7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; hu-HU; rv:1.7.12) Gecko/20051010 Firefox/\
     1.0.7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.12) Gecko/20051010 Firefox/1.0\
     .7 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.12) Gecko/20050922 Firefox/1.0\
     .7 (Debian package 1.0.7-1)",
    "Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.12) Gecko/20050922 Fedora/1.0.\
     7-1.1.fc4 Firefox/1.0.7",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.7.10) Gecko/20050919 (No ID\
     N) Firefox/1.0.6",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.10) Gecko/20050724 Firefo\
     x/1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; pt-BR; rv:1.7.10) Gecko/20050717 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.7.10) Gecko/20050730 Firefox/\
     1.0.6 (Debian package 1.0.6-2)",
    "Mozilla/5.0 (X11; U; Linux i686; pl-PL; rv:1.7.10) Gecko/20050717 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.10) Gecko/20050721 Firefox/1.0\
     .6 (Ubuntu package 1.0.6)",
    "Mozilla/5.0 (X11; U; Linux i686; fr-FR; rv:1.7.10) Gecko/20050716 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20051111 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20051106 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050920 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050918 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050911 Firefox/\
     1.0.6 (Debian package 1.0.6-5)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050815 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050811 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050721 Firefox/\
     1.0.6 (Ubuntu package 1.0.6)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050720 Fedora/1\
     .0.6-1.1.fc4.k12ltsp.4.4.0 Firefox/1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050720 Fedora/1\
     .0.6-1.1.fc3 Firefox/1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050719 Red Hat/\
     1.0.6-1.4.1 Firefox/1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050716 Firefox/\
     1.0.6",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20050715 Firefox/\
     1.0.6 SUSE/1.0.6-16",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.9) Gecko/20050711 Firefox/1\
     .0.5",
	"Mozilla/5.0 (Windows; U; WinNT4.0; en-US; rv:1.7.9) Gecko/20050711 Firefox\
     /1.0.5",
	"Mozilla/5.0 (Windows; U; Windows NT5.1; en; rv:1.7.10) Gecko/20050716 Fire\
     fox/1.0.5",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US; rv:1.7.9) Gecko/20050711 F\
     irefox/1.0.5",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.7.10) Gecko/20050716 Fir\
     efox/1.0.5",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.9) Gecko/20050711 F\
     irefox/1.0.5 (ax)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.9) Gecko/20050711 F\
     irefox/1.0.5",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.9) Gecko/20050711 F\
     irefox/1.0.5",
	"Mozilla/5.0 (Windows; U; Win 9x 4.90; en-US; rv:1.7.9) Gecko/20050711 Fire\
     fox/1.0.5",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.7.9) Gecko/200\
     50711 Firefox/1.0.5",
	"Mozilla/5.0 (X11; U; SunOS sun4u; en-US; rv:1.7.8) Gecko/20050512 Firefox/\
     1.0.4",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.8) Gecko/20050511 Firefox\
     /1.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.8) Gecko/20050524 Fedora/1.0.4\
     -4 Firefox/1.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.7.10) Gecko/20050925 Firefox/1.0\
     .4 (Debian package 1.0.4-2sarge5)",
    "Mozilla/5.0 (X11; U; Linux i686; fr-FR; rv:1.7.8) Gecko/20050511 Firefox/1\
     .0.4",
	"Mozilla/5.0 (X11; U; Linux i686; fr-FR; rv:1.7.10) Gecko/20050925 Firefox/\
     1.0.4 (Debian package 1.0.4-2sarge5)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050610 Firefox/1\
     .0.4 (Debian package 1.0.4-3)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050524 Fedora/1.\
     0.4-4 Firefox/1.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050523 Firefox/1\
     .0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050517 Firefox/1\
     .0.4 (Debian package 1.0.4-2)",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050513 Firefox/1\
     .0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050513 Fedora/1.\
     0.4-1.3.1 Firefox/1.0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050512 Firefox/1\
     .0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050511 Firefox/1\
     .0.4 SUSE/1.0.4-1.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.8) Gecko/20050511 Firefox/1\
     .0.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.12) Gecko/20051010 Firefox/\
     1.0.4 (Ubuntu package 1.0.7)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20070530 Firefox/\
     1.0.4 (Debian package 1.0.4-2sarge17)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20070116 Firefox/\
     1.0.4 (Debian package 1.0.4-2sarge15)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20061113 Firefox/\
     1.0.4 (Debian package 1.0.4-2sarge13)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.10) Gecko/20060927 Firefox/\
     1.0.4 (Debian package 1.0.4-2sarge12)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.7) Gecko/20050421 Firefox/1\
     .0.3 (Debian package 1.0.3-2)",
    "Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.7.7) Gecko/20050414 Firefox/1\
     .0.3",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.7.7) Gecko/20060303 Firefox\
     /1.0.3",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.7.7) Gecko/20050420 Firefox\
     /1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it-IT; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr-FR; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3 (ax)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; da-DK; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; fr-FR; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7.7) Gecko/20050414 F\
     irefox/1.0.3",
	"Mozilla/5.0 (Windows; U; Win98; fr-FR; rv:1.7.7) Gecko/20050414 Firefox/1.\
     0.3",
	"Mozilla/5.0 (Windows; U; Win98; es-ES; rv:1.7.7) Gecko/20050414 Firefox/1.\
     0.3",
	"Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.\
     0.3",
	"Mozilla/5.0 (Windows; U; Win98; de-DE; rv:1.7.7) Gecko/20050414 Firefox/1.\
     0.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; nl-NL; rv:1.7.6) Gecko/20050318 Firefox\
     /1.0.2",
	"Mozilla/5.0 (X11; U; Linux i686; ru-RU; rv:1.7.6) Gecko/20050318 Firefox/1\
     .0.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050317 Firefox/1\
     .0.2",
	"Mozilla/5.0 (X11; U; Linux i686; de-AT; rv:1.7.6) Gecko/20050325 Firefox/1\
     .0.2 (Debian package 1.0.2-1)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.2; de-DE; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; tr-TR; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; sv-SE; rv:1.7.6) Gecko/20050318 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; ro-RO; rv:1.7.6) Gecko/20050318 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; nl-NL; rv:1.7.6) Gecko/20050318 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; it-IT; rv:1.7.6) Gecko/20050318 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr-FR; rv:1.7.6) Gecko/20050318 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.6) Gecko/20050317 F\
     irefox/1.0.2 (ax)",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.6) Gecko/20050317 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.6) Gecko/20050317 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7.6) Gecko/20050321 F\
     irefox/1.0.2",
	"Mozilla/5.0 (Windows; U; Win98; fr-FR; rv:1.7.6) Gecko/20050318 Firefox/1.\
     0.2",
	"Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.7.6) Gecko/20050317 Firefox/1.\
     0.2 (ax)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050317 Firefox/1\
     .0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050311 Firefox/1\
     .0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050310 Firefox/1\
     .0.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050225 Firefox/1\
     .0.1",
	"Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.7.6) Gecko/20050322 Firefox/1\
     .0.1",
	"Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.7.6) Gecko/20050306 Firefox/1\
     .0.1 (Debian package 1.0.1-2)",
    "Mozilla/5.0 (X11; U; Linux i686; cs-CZ; rv:1.7.6) Gecko/20050226 Firefox/1\
     .0.1",
	"Mozilla/5.0 (Windows; U; WinNT4.0; de-DE; rv:1.7.6) Gecko/20050226 Firefox\
     /1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr-FR; rv:1.7.6) Gecko/20050226 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.6) Gecko/20050225 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.6) Gecko/20050223 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.7.6) Gecko/20050226 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7.6) Gecko/20050226 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7.6) Gecko/20050223 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.6) Gecko/20050225 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7.6) Gecko/20050226 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7.6) Gecko/20050223 F\
     irefox/1.0.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.6) Gecko/20040206 Fir\
     efox/1.0.1",
	"Mozilla/5.0 (Windows; U; Win98; fr-FR; rv:1.7.6) Gecko/20050226 Firefox/1.\
     0.1",
	"Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.7.6) Gecko/20050225 Firefox/1.\
     0.1",
	"Mozilla/5.0 (X11; U; Linux i686; hu; rv:1.8b4) Gecko/20050827 Firefox/1.0+\
     ",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8b4) Gecko/20050729 F\
     irefox/1.0+",
	"Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.7.5) Gecko/20041109 Firefox/\
     1.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.7.6) Gecko/20050405 Firefox\
     /1.0 (Ubuntu package 1.0.2)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050405 Firefox/1\
     .0 (Ubuntu package 1.0.2)",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20050814 Firefox/1\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20050221 Firefox/1\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20050210 Firefox/1\
     .0 (Debian package 1.0+dfsg.1-6)",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041218 Firefox/1\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041215 Firefox/1\
     .0 Red Hat/1.0-12.EL4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041204 Firefox/1\
     .0 (Debian package 1.0.x.2-1)",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041128 Firefox/1\
     .0 (Debian package 1.0-4)",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041117 Firefox/1\
     .0 (Debian package 1.0-2.0.0.45.linspire0.4)",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.5) Gecko/20041107 Firefox/1\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; en-GB; rv:1.7.6) Gecko/20050405 Firefox/1\
     .0 (Ubuntu package 1.0.2)",
    "Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.7.5) Gecko/20041108 Firefox/1\
     .0",
	"Mozilla/5.0 (X11; U; Linux i686; de-AT; rv:1.7.5) Gecko/20041128 Firefox/1\
     .0 (Debian package 1.0-4)",
    "Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.7.5) Gecko/20041114 Firefox\
     /1.0",
	"Mozilla/5.0 (X11; Linux i686; rv:1.7.5) Gecko/20041108 Firefox/1.0",
	"Mozilla/5.0 (Windows; U; WinNT4.0; en-US; rv:1.7.5) Gecko/20041107 Firefox\
     /1.0",
	"Mozilla/5.0 (Windows; U; WinNT4.0; de-DE; rv:1.7.5) Gecko/20041108 Firefox\
     /1.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-TW; rv:1.7.5) Gecko/20041119 F\
     irefox/1.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040917 Firefox/0.9\
     .3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.7) Gecko/20040803 Firefo\
     x/0.9.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040803 Fir\
     efox/0.9.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7) Gecko/20040803 Fir\
     efox/0.9.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7) Gecko/20040803 Fir\
     efox/0.9.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7) Gecko/20040803 Fir\
     efox/0.9.3",
	"Mozilla/5.0 (Windows; U; Win98; de-DE; rv:1.7) Gecko/20040803 Firefox/0.9.\
     3",
	"Mozilla/5.0 (Windows; U; Win 9x 4.90; rv:1.7) Gecko/20040803 Firefox/0.9.3\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040802 Firefox/0.9\
     .2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.7) Gecko/20040707 Firefo\
     x/0.9.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040707 Fir\
     efox/0.9.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7) Gecko/20040707 Fir\
     efox/0.9.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7) Gecko/20040630 Firefox/0.9\
     .1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.7) Gecko/20040626 Fir\
     efox/0.9.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.7) Gecko/20040626 Fir\
     efox/0.9.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040614 Fir\
     efox/0.9",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.7) Gecko/20040\
     614 Firefox/0.9",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.6) Gecko/20040614 Firefox/0.8\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.6) Gecko/20040225 Firefox/0.8\
     ",
	"Mozilla/5.0 (X11; U; Linux i686; de-DE; rv:1.6) Gecko/20040207 Firefox/0.8\
     ",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.6) Gecko/20040206 Firefo\
     x/0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Fir\
     efox/0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.6) Gecko/20040206 Fir\
     efox/0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.6) Gecko/20040206 Fir\
     efox/0.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.6) Gecko/20040206 Fir\
     efox/0.8",
	"Mozilla/5.0 (Windows; U; Win98; en-US; rv:1.6) Gecko/20040206 Firefox/0.8",
	"Mozilla/5.0 (Windows; U; Win 9x 4.90; en-US; rv:1.6) Gecko/20040206 Firefo\
     x/0.8",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.6) Gecko/20040\
     206 Firefox/0.8",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.7.3) Gecko/20041020 Firefox/0.10.1",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.7.3) Gecko/20041001 Firefox/0.10.1",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.7.3) Gecko/20040914 Firefox/0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; rv:1.7.3) Gecko/20041001 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040911 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; rv:1.7.3) Gecko/20041001 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; rv:1.7.3) Gecko/20040913 Firefox/\
     0.10.1",
	"Mozilla/5.0 (Windows; U; Win98; rv:1.7.3) Gecko/20041001 Firefox/0.10.1",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.7.3) Gecko/20040914 Firefox/0.10",
	"Mozilla/5.0 (X11; U; Linux i686; rv:1.7.3) Gecko/20040913 Firefox/0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20040913 Firefox/\
     0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; zh-TW; rv:1.8.0.1) Gecko/20060111\
     Firefox/0.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; rv:1.7.3) Gecko/20040913 Firefox/\
     0.10",
	"Mozilla/5.0 (Windows; U; Win98; rv:1.7.3) Gecko/20040913 Firefox/0.10",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; rv:1.7.3) Gecko/20040913 F\
     irefox/0.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.19) Gecko/20081202 Firefo\
     x (Debian-2.0.0.19-0etch1)",
    "Mozilla/5.0 (X11; U; Gentoo Linux x86_64; pl-PL) Gecko Firefox",
	"Mozilla/5.0 (X11; ; Linux x86_64; rv:1.8.1.6) Gecko/20070802 Firefox",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.6) Gecko/20090119\
     13 Firefox",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE; rv:1.9.2.20) Gecko/2011080\
     3 Firefox",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; rv:1.8.1.16) Gecko/2008070\
     2 Firefox",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1.13) Gecko/20080\
     313 Firefox",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/109.0.5398.133 Chrome/109.0.5398.133 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.12.9094) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/103.0.5060.129 Chrome/103.0.5060.129 Safar\
     i/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.12.9076) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/103.0.5060.129 Chrome/103.0.5060.129 Safar\
     i/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.11.9118) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.4967.149 Chrome/104.0.4967.149 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.11.9114) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.10.9120) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/89.0.4389.105 Chrome/89.0.4389.105 Safari/\
     537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5138.119 Chrome/102.0.5138.119 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/104.0.5127.199 Chrome/104.0.5127.199 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/102.0.5143.178 Chrome/102.0.5143.178 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5048.207 Chrome/102.0.5048.207 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5022.200 Chrome/102.0.5022.200 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/102.0.5134.171 Chrome/102.0.5134.171 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.5038.199 Chrome/103.0.5038.199 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.11.9106) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.4989.94 Chrome/101.0.4989.94 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5051.149 Chrome/102.0.5051.149 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.12.2144) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (Linux; Android 11; SMARTEMB) AppleWebKit/537.36 (KHTML, like \
     Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5112.90 Chrome/102.0.5112.90 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/103.0.5104.184 Chrome/103.0.5104.184 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5158.124 Chrome/104.0.5158.124 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/103.0.5115.128 Chrome/103.0.5115.128 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/103.0.5113.129 Chrome/103.0.5113.129 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5035.200 Chrome/104.0.5035.200 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/103.0.5083.93 Chrome/103.0.5083.93 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.4996.137 Chrome/102.0.4996.137 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.5064.193 Chrome/101.0.5064.193 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.5052.138 Chrome/101.0.5052.138 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5021.93 Chrome/104.0.5021.93 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/103.0.5077.173 Chrome/103.0.5077.173 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/104.0.5145.133 Chrome/104.0.5145.133 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/102.0.4962.148 Chrome/102.0.4962.148 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.5036.118 Chrome/101.0.5036.118 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/104.0.4965.107 Chrome/104.0.4965.107 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5147.148 Chrome/104.0.5147.148 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/103.0.5047.196 Chrome/103.0.5047.196 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.5144.103 Chrome/101.0.5144.103 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/102.0.5065.88 Chrome/102.0.5065.88 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.5027.169 Chrome/101.0.5027.169 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/104.0.5084.197 Chrome/104.0.5084.197 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/103.0.5056.80 Chrome/103.0.5056.80 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.4978.176 Chrome/104.0.4978.176 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/101.0.5071.127 Chrome/101.0.5071.127 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5022.103 Chrome/104.0.5022.103 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.4983.125 Chrome/104.0.4983.125 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.5106.117 Chrome/103.0.5106.117 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Ubunt\
     u/10.04 Chromium/17.0.963.56 Chrome/17.0.963.56 Safari/535.11",
	"Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML, like Gecko) sna\
     p Chromium/78.0.3904.70 Chrome/78.0.3904.70 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.11.9102) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/102.0.5109.187 Chrome/102.0.5109.187 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.5138.148 Chrome/104.0.5138.148 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.5144.183 Chrome/101.0.5144.183 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.4992.93 Chrome/102.0.4992.93 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/103.0.5132.95 Chrome/103.0.5132.95 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/104.0.4991.177 Chrome/104.0.4991.177 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/102.0.5132.163 Chrome/102.0.5132.163 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5153.205 Chrome/102.0.5153.205 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5080.102 Chrome/102.0.5080.102 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/104.0.5022.157 Chrome/104.0.5022.157 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/104.0.5008.159 Chrome/104.0.5008.159 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.5093.88 Chrome/103.0.5093.88 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/104.0.5049.206 Chrome/104.0.5049.206 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/103.0.5060.53 Chrome/103.0.5060.53 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/103.0.5008.98 Chrome/103.0.5008.98 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.5076.207 Chrome/101.0.5076.207 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/102.0.5135.94 Chrome/102.0.5135.94 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.4951.155 Chrome/103.0.4951.155 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.4987.212 Chrome/101.0.4987.212 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.5038.83 Chrome/103.0.5038.83 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/102.0.4969.188 Chrome/102.0.4969.188 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/104.0.4980.109 Chrome/104.0.4980.109 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/104.0.5072.175 Chrome/104.0.5072.175 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.5030.202 Chrome/102.0.5030.202 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.4986.105 Chrome/103.0.4986.105 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64; SMARTEMB Build/3.11.9076) AppleWebKit/537.\
     36 (KHTML, like Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/\
     537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/101.0.5094.87 Chrome/101.0.5094.87 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/102.0.4979.196 Chrome/102.0.4979.196 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.5069.174 Chrome/101.0.5069.174 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/103.0.5092.97 Chrome/103.0.5092.97 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/102.0.4990.115 Chrome/102.0.4990.115 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.4857.204 Chrome/101.0.4857.204 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/100.0.4896.115 Chrome/100.0.4896.115 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/101.0.4869.122 Chrome/101.0.4869.122 Safari/537.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubunt\
     u Chromium/99.0.4857.173 Chrome/99.0.4857.173 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/99.0.4852.96 Chrome/99.0.4852.96 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/37.0.2062.120 Chrome/37.0.2062.120 Safari/9EA227",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/99.0.4881.118 Chrome/99.0.4881.118 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/101.0.4873.165 Chrome/101.0.4873.165 Safari/537.36",
	"Mozilla/5.0 (Linux; Android 7.1.2; SMARTEMB) AppleWebKit/537.36 (KHTML, li\
     ke Gecko) Chromium/98.0.4758.101 Chrome/98.0.4758.101 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.4847.193 Chrome/101.0.4847.193 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ub\
     untu Chromium/100.0.4881.115 Chrome/100.0.4881.115 Safari/537.36",
	"Mozilla/5.0 (X11; U; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) \
     Ubuntu Chromium/101.0.4857.184 Chrome/101.0.4857.184 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubu\
     ntu Chromium/99.0.4862.111 Chrome/99.0.4862.111 Safari/537.36",
	"Mozilla/5.0 (X11; FreeBSD i386) AppleWebKit/535.2 (KHTML, like Gecko) Chro\
     me/15.0.874.121 Safari/535.2",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.2 (KHTML, like Gecko) Ubuntu\
     /11.10 Chromium/15.0.874.120 Chrome/15.0.874.120 Safari/535.2",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/\
     15.0.874.120 Safari/535.2",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/\
     15.0.872.0 Safari/535.2",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.2 (KHTML, like Gecko) Ubun\
     tu/11.04 Chromium/15.0.871.0 Chrome/15.0.871.0 Safari/535.2",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/\
     15.0.864.0 Safari/535.2",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/\
     15.0.861.0 Safari/535.2",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.2 (KHTML, l\
     ike Gecko) Chrome/15.0.861.0 Safari/535.2",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.2 (KHTML, l\
     ike Gecko) Chrome/15.0.861.0 Safari/535.2",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/\
     15.0.860.0 Safari/535.2",
	"Chrome/15.0.860.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.20.2\
     5 (KHTML, like Gecko) Version/15.0.860.0",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.835.186 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.834.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /11.04 Chromium/14.0.825.0 Chrome/14.0.825.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/14.0.824.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.815.10913 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.815.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /11.04 Chromium/14.0.814.0 Chrome/14.0.814.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/14.0.814.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /10.04 Chromium/14.0.813.0 Chrome/14.0.813.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/14.0.813.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.813.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.813.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.813.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.812.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/14.0.811.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/14.0.810.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.810.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.809.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Ubun\
     tu/10.10 Chromium/14.0.808.0 Chrome/14.0.808.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /10.04 Chromium/14.0.808.0 Chrome/14.0.808.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /10.04 Chromium/14.0.804.0 Chrome/14.0.804.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Ubuntu\
     /11.04 Chromium/14.0.803.0 Chrome/14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Chrome\
     /14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.803.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.801.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.801.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.794.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.794.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.792.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.792.0 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     14.0.792.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; PPC Mac OS X 10_6_7) AppleWebKit/535.1 (KHTML, lik\
     e Gecko) Chrome/14.0.790.0 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/14.0.790.0 Safari/535.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1) AppleWebKit/526.3 (KHTML, like Ge\
     cko) Chrome/14.0.564.21 Safari/526.3",
	"Mozilla/5.0 (X11; CrOS i686 13.587.48) AppleWebKit/535.1 (KHTML, like Geck\
     o) Chrome/13.0.782.43 Safari/535.1",
	"Mozilla/5.0 Slackware/13.37 (X11; U; Linux x86_64; en-US) AppleWebKit/535.\
     1 (KHTML, like Gecko) Chrome/13.0.782.41",
	"Mozilla/5.0 ArchLinux (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like G\
     ecko) Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Ubun\
     tu/11.04 Chromium/13.0.782.41 Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Chrome\
     /13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_3) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_2) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.41 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_3) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.32 Safari/535.1",
	"Mozilla/5.0 (X11; Linux amd64) AppleWebKit/535.1 (KHTML, like Gecko) Chrom\
     e/13.0.782.24 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/13.0.782.24 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.24 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/13.0.782.220 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) \
     Chrome/13.0.782.220 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.220 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/13.0.782.215 Safari/535.1",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.1 (KHTML, like Gecko) Chrome\
     /13.0.782.215 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.215 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.215 Safari/535.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/535.1 (KHTML, like G\
     ecko) Chrome/13.0.782.20 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chro\
     me/13.0.782.20 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.20 Safari/535.1",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.20 Safari/535.1",
	"Mozilla/5.0 (X11; CrOS i686 0.13.587) AppleWebKit/535.1 (KHTML, like Gecko\
     ) Chrome/13.0.782.14 Safari/535.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/535.1 (KHTML, \
     like Gecko) Chrome/13.0.782.107 Safari/535.1",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_2) AppleWebKit/535.1 (KHTML, l\
     ike Gecko) Chrome/13.0.782.107 Safari/535.1",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/\
     13.0.782.1 Safari/535.1",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.36 (KHTML, like Gecko) Chr\
     ome/13.0.766.0 Safari/534.36",
	"Mozilla/5.0 (X11; Linux amd64) AppleWebKit/534.36 (KHTML, like Gecko) Chro\
     me/13.0.766.0 Safari/534.36",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.35 (KHTML, like Gecko) Ubunt\
     u/10.10 Chromium/13.0.764.0 Chrome/13.0.764.0 Safari/534.35",
	"Mozilla/5.0 (X11; CrOS i686 0.13.507) AppleWebKit/534.35 (KHTML, like Geck\
     o) Chrome/13.0.763.0 Safari/534.35",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.33 (KHTML, like Gecko) Ubunt\
     u/9.10 Chromium/13.0.752.0 Chrome/13.0.752.0 Safari/534.33",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/534.31 (KHTML, \
     like Gecko) Chrome/13.0.748.0 Safari/534.31",
	"Mozilla/5.0 (Windows NT 6.1; en-US) AppleWebKit/534.30 (KHTML, like Gecko)\
     Chrome/12.0.750.0 Safari/534.30",
	"Mozilla/5.0 (X11; CrOS i686 12.433.109) AppleWebKit/534.30 (KHTML, like Ge\
     cko) Chrome/12.0.742.93 Safari/534.30",
	"Mozilla/5.0 (X11; CrOS i686 12.0.742.91) AppleWebKit/534.30 (KHTML, like G\
     ecko) Chrome/12.0.742.93 Safari/534.30",
	"Mozilla/5.0 Slackware/13.37 (X11; U; Linux x86_64; en-US) AppleWebKit/534.\
     16 (KHTML, like Gecko) Chrome/12.0.742.91",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Chrom\
     e/12.0.742.91 Chromium/12.0.742.91 Safari/534.30",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, \
     like Gecko) Chrome/12.0.742.68 Safari/534.30",
	"Mozilla/5.0 ArchLinux (X11; U; Linux x86_64; en-US) AppleWebKit/534.30 (KH\
     TML, like Gecko) Chrome/12.0.742.60 Safari/534.30",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.30 (KHTML, like Gecko)\
     Chrome/12.0.742.53 Safari/534.30",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome\
     /12.0.742.113 Safari/534.30",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubu\
     ntu/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubu\
     ntu/10.10 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.30 (KHTML, like Gecko) Ubu\
     ntu/10.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Ubunt\
     u/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Ubunt\
     u/10.10 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Ubunt\
     u/10.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (Windows NT 7.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome\
     /12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (Windows NT 5.2) AppleWebKit/534.30 (KHTML, like Gecko) Chrome\
     /12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (Windows 8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0\
     .742.112 Safari/534.30",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/534.30 (KHTML, \
     like Gecko) Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_4) AppleWebKit/534.30 (KHTML, \
     like Gecko) Chrome/12.0.742.112 Safari/534.30",
	"Mozilla/5.0 (X11; CrOS i686 12.433.216) AppleWebKit/534.30 (KHTML, like Ge\
     cko) Chrome/12.0.742.105 Safari/534.30",
	"Mozilla/5.0 ArchLinux (X11; U; Linux x86_64; en-US) AppleWebKit/534.30 (KH\
     TML, like Gecko) Chrome/12.0.742.100 Safari/534.30",
	"Mozilla/5.0 ArchLinux (X11; U; Linux x86_64; en-US) AppleWebKit/534.30 (KH\
     TML, like Gecko) Chrome/12.0.742.100",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Slack\
     ware/Chrome/12.0.742.100 Safari/534.30",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Chrom\
     e/12.0.742.100 Safari/534.30",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/534.30 (KHTML, like Gecko) Chrome\
     /12.0.742.100 Safari/534.30",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/534.30 (KHTML, \
     like Gecko) Chrome/12.0.742.100 Safari/534.30",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_4) AppleWebKit/534.30 (KHTML, \
     like Gecko) Chrome/12.0.742.100 Safari/534.30",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.30 (KHTML,\
     like Gecko) Chrome/12.0.724.100 Safari/534.30",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.25 (KHTML, like Gecko) Chrome\
     /12.0.706.0 Safari/534.25",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.25 (KHTML, like Gecko) Chrome\
     /12.0.704.0 Safari/534.25",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Ubu\
     ntu/10.10 Chromium/12.0.703.0 Chrome/12.0.703.0 Safari/534.24",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Ubunt\
     u/10.10 Chromium/12.0.702.0 Chrome/12.0.702.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.24 (KHTML, like Gecko)\
     Chrome/12.0.702.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /12.0.702.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.700.3 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.699.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko)\
     Chrome/11.0.699.0 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.698.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.697.0 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.696.71 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.696.68 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_7) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.696.68 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_8) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.696.68 Safari/534.24",
	"Mozilla/5.0 Slackware/13.37 (X11; U; Linux x86_64; en-US) AppleWebKit/534.\
     16 (KHTML, like Gecko) Chrome/11.0.696.50",
	"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.696.43 Safari/534.24",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chr\
     ome/11.0.696.34 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/534.24 (KHTML, like Gecko)\
     Chrome/11.0.696.34 Safari/534.24",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Chr\
     ome/11.0.696.3 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.696.3 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.696.3 Safari/534.24",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.24 (KHTML, like Gecko) Chrom\
     e/11.0.696.14 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.24 (KHTML, like Gecko)\
     Chrome/11.0.696.12 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_6) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.696.12 Safari/534.24",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/534.24 (KHTML, like Gecko) Ubu\
     ntu/10.04 Chromium/11.0.696.0 Chrome/11.0.696.0 Safari/534.24",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/534.24 (KHTML, \
     like Gecko) Chrome/11.0.696.0 Safari/534.24",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome\
     /11.0.694.0 Safari/534.24",
	"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.23 (KHTML, like Gecko) Chrom\
     e/11.0.686.3 Safari/534.23",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.21 (KHTML,\
     like Gecko) Chrome/11.0.682.0 Safari/534.21",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.21 (KHTML,\
     like Gecko) Chrome/11.0.678.0 Safari/534.21",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_7_0; en-US) AppleWebKit/534.2\
     1 (KHTML, like Gecko) Chrome/11.0.678.0 Safari/534.21",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML,\
     like Gecko) Chrome/11.0.672.2 Safari/534.20",
	"Mozilla/5.0 (Windows NT) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.\
     0.672.2 Safari/534.20",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.2\
     0 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.20 (KHTML,\
     like Gecko) Chrome/11.0.669.0 Safari/534.20",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.19 (KHTML,\
     like Gecko) Chrome/11.0.661.0 Safari/534.19",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.18 (KHTML,\
     like Gecko) Chrome/11.0.661.0 Safari/534.18",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.1\
     8 (KHTML, like Gecko) Chrome/11.0.660.0 Safari/534.18",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML,\
     like Gecko) Chrome/11.0.655.0 Safari/534.17",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     7 (KHTML, like Gecko) Chrome/11.0.655.0 Safari/534.17",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML,\
     like Gecko) Chrome/11.0.654.0 Safari/534.17",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.17 (KHTML,\
     like Gecko) Chrome/11.0.652.0 Safari/534.17",
	"Mozilla/4.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like G\
     ecko) Chrome/11.0.1245.0 Safari/537.36",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.17 (KHTML,\
     like Gecko) Chrome/10.0.649.0 Safari/534.17",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.17 (KHTML,\
     like Gecko) Chrome/10.0.649.0 Safari/534.17",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.82 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux armv7l; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.204 Safari/534.16",
	"Mozilla/5.0 (X11; U; FreeBSD x86_64; en-US) AppleWebKit/534.16 (KHTML, lik\
     e Gecko) Chrome/10.0.648.204 Safari/534.16",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.204 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.204",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Ge\
     cko) Chrome/10.0.648.134 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.648.134 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.648.134 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.134 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/10.0.648.133 Chrome/10.0.648.133 Safari/534.1\
     6",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Ge\
     cko) Ubuntu/10.10 Chromium/10.0.648.133 Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Ge\
     cko) Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/10.0.648.127 Chrome/10.0.648.127 Safari/534.1\
     6",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.127 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.127 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.127 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Chrome/10.0.648.11 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; ru-RU; AppleWebKit/534.16; KHTML;\
     like Gecko; Chrome/10.0.648.11;Safari/534.16)",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; ru-RU) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.648.11 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.648.11 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.16 (KHTML, like Ge\
     cko) Ubuntu/10.10 Chromium/10.0.648.0 Chrome/10.0.648.0 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.648.0 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/10.0.642.0 Chrome/10.0.642.0 Safari/534.16",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.1\
     6 (KHTML, like Gecko) Chrome/10.0.639.0 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.638.0 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/534.16 (KHTML\
     , like Gecko) Chrome/10.0.634.0 Safari/534.16",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.16 (KHTML,\
     like Gecko) Chrome/10.0.634.0 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.16 SUSE/10.0.626\
     .0 (KHTML, like Gecko) Chrome/10.0.626.0 Safari/534.16",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.15 (KHTML, like \
     Gecko) Chrome/10.0.613.0 Safari/534.15",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Ge\
     cko) Ubuntu/10.10 Chromium/10.0.613.0 Chrome/10.0.613.0 Safari/534.15",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Ge\
     cko) Ubuntu/10.04 Chromium/10.0.612.3 Chrome/10.0.612.3 Safari/534.15",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Ge\
     cko) Chrome/10.0.612.1 Safari/534.15",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.15 (KHTML, like Ge\
     cko) Ubuntu/10.10 Chromium/10.0.611.0 Chrome/10.0.611.0 Safari/534.15",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML,\
     like Gecko) Chrome/10.0.602.0 Safari/534.14",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.14 (KHTML,\
     like Gecko) Chrome/10.0.601.0 Safari/534.14",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML,\
     like Gecko) Chrome/10.0.601.0 Safari/534.14",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Ge\
     cko) Chrome/9.1.0.0 Safari/540.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML, like G\
     ecko) Ubuntu/10.10 Chrome/9.1.0.0 Safari/540.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.14 (KHTML,\
     like Gecko) Chrome/9.0.601.0 Safari/534.14",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.14 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/9.0.600.0 Chrome/9.0.600.0 Safari/534.14",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML,\
     like Gecko) Chrome/9.0.600.0 Safari/534.14",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.599.0 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-CA) AppleWebKit/534.13 (KHTML \
     like Gecko) Chrome/9.0.597.98 Safari/534.13",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Ge\
     cko) Chrome/9.0.597.84 Safari/534.13",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Ge\
     cko) Chrome/9.0.597.44 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.597.19 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.597.15 Safari/534.13",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.1\
     3 (KHTML, like Gecko) Chrome/9.0.597.15 Safari/534.13",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.107 Safari/534.13 v1416758524.9051",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.107 Safari/534.13 v1416748405.3871",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.107 Safari/534.13 v1416670950.695",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.107 Safari/534.13 v1416664997.4379",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.107 Safari/534.13 v1333515017.9196",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)  AppleWebKit/534.13 (KHTML\
     , like Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.1\
     3 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     3 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML,\
     like Gecko) Chrome/9.0.596.0 Safari/534.13",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.13 (KHTML, like \
     Gecko) Ubuntu/10.04 Chromium/9.0.595.0 Chrome/9.0.595.0 Safari/534.13",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.13 (KHTML, like Ge\
     cko) Ubuntu/9.10 Chromium/9.0.592.0 Chrome/9.0.592.0 Safari/534.13",
	"Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like \
     Gecko) Chrome/9.0.587.0 Safari/534.12",
	"Mozilla/5.0 (Windows  U  Windows NT 5.1  en-US) AppleWebKit/534.12 (KHTML,\
     like Gecko) Chrome/9.0.583.0 Safari/534.12",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.12 (KHTML, like Ge\
     cko) Chrome/9.0.579.0 Safari/534.12",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/534.12 (KHTML\
     , like Gecko) Chrome/9.0.576.0 Safari/534.12",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML, like G\
     ecko) Ubuntu/10.10 Chrome/8.1.0.0 Safari/540.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/8.0.558.0 Safari/534.10",
	"Mozilla/5.0 (X11; U; CrOS i686 0.9.130; en-US) AppleWebKit/534.10 (KHTML, \
     like Gecko) Chrome/8.0.552.344 Safari/534.10",
	"Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, \
     like Gecko) Chrome/8.0.552.343 Safari/534.10",
	"Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, \
     like Gecko) Chrome/8.0.552.341 Safari/534.10",
	"Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, \
     like Gecko) Chrome/8.0.552.339 Safari/534.10",
	"Mozilla/5.0 (X11; U; CrOS i686 0.9.128; en-US) AppleWebKit/534.10 (KHTML, \
     like Gecko) Chrome/8.0.552.339",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like \
     Gecko) Ubuntu/10.10 Chromium/8.0.552.237 Chrome/8.0.552.237 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/8.0.552.224 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/8.0.552.224 Safari/533.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-US) AppleWebKit/534.1\
     0 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.1\
     0 (KHTML, like Gecko) Chrome/8.0.552.224 Safari/534.10",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like \
     Gecko) Chrome/8.0.552.215 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/8.0.552.215 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/8.0.552.215 Safari/534.10",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     0 (KHTML, like Gecko) Chrome/8.0.552.210 Safari/534.10",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like \
     Gecko) Chrome/8.0.552.200 Safari/534.10",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.10 (KHTML, like Ge\
     cko) Chrome/8.0.551.0 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/7.0.548.0 Safari/534.10",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.10 (KHTML, like \
     Gecko) Chrome/7.0.544.0 Safari/534.10",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.15) Gecko/20101027 Mozi\
     lla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, lik\
     e Gecko) Chrome/7.0.540.0 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/7.0.540.0 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; de-DE) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/7.0.540.0 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML,\
     like Gecko) Chrome/7.0.540.0 Safari/534.10",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.9 (KHTML, \
     like Gecko) Chrome/7.0.531.0 Safari/534.9",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.8 (KHTML, \
     like Gecko) Chrome/7.0.521.0 Safari/534.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.7 (KHTML, like Gec\
     ko) Chrome/7.0.517.24 Safari/534.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like G\
     ecko) Chrome/7.0.514.0 Safari/534.7",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.7 (KHTML, like G\
     ecko) Chrome/7.0.514.0 Safari/534.7",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.7 (KHTML, \
     like Gecko) Chrome/7.0.514.0 Safari/534.7",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.6 (KHTML, \
     like Gecko) Chrome/7.0.500.0 Safari/534.6",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.6 (KHTML, \
     like Gecko) Chrome/7.0.498.0 Safari/534.6",
	"Mozilla/5.0 (ipad Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.6 (KH\
     TML, like Gecko) Chrome/7.0.498.0 Safari/534.6",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML,\
     like Gecko) Chrome/7.0.0 Safari/700.13",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, \
     like Gecko) Chrome/6.0.481.0 Safari/534.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/534.3\
     (KHTML, like Gecko) Chrome/6.0.472.63 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.472.53 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.472.33 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.3 (KHTML, like G\
     ecko) Chrome/6.0.470.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.464.0 Safari/534.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3\
     (KHTML, like Gecko) Chrome/6.0.464.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.463.0 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gec\
     ko) Chrome/6.0.462.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.462.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.461.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.461.0 Safari/534.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3\
     (KHTML, like Gecko) Chrome/6.0.461.0 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gec\
     ko) Chrome/6.0.460.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.460.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.460.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.459.0 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.3 (KHTML, like G\
     ecko) Chrome/6.0.458.1 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.458.1 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.458.1 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.458.1 Safari/534.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3\
     (KHTML, like Gecko) Chrome/6.0.458.1 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gec\
     ko) Chrome/6.0.458.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.3 (KHTML, \
     like Gecko) Chrome/6.0.458.0 Safari/534.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.3 (KHTML, like Gec\
     ko) Chrome/6.0.457.0 Safari/534.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/534.3\
     (KHTML, like Gecko) Chrome/6.0.456.0 Safari/534.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.2 (KHTML, \
     like Gecko) Chrome/6.0.454.0 Safari/534.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.2 (KHTML, \
     like Gecko) Chrome/6.0.454.0 Safari/534.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.2 (KHTML, like Gec\
     ko) Chrome/6.0.453.1 Safari/534.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/534.2\
     (KHTML, like Gecko) Chrome/6.0.453.1 Safari/534.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.2\
     (KHTML, like Gecko) Chrome/6.0.453.1 Safari/534.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.2\
     (KHTML, like Gecko) Chrome/6.0.451.0 Safari/534.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.1 SUSE/6.0.428.0 (\
     KHTML, like Gecko) Chrome/6.0.428.0 Safari/534.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.1 (KHTML, \
     like Gecko) Chrome/6.0.428.0 Safari/534.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB) AppleWebKit/534.1 (KHTML, \
     like Gecko) Chrome/6.0.428.0 Safari/534.1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/534.1\
     (KHTML, like Gecko) Chrome/6.0.428.0 Safari/534.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.1 (KHTML, like G\
     ecko) Chrome/6.0.427.0 Safari/534.1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/534.1\
     (KHTML, like Gecko) Chrome/6.0.422.0 Safari/534.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.1 (KHTML, like G\
     ecko) Chrome/6.0.417.0 Safari/534.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/534.1 (KHTML, like Gec\
     ko) Chrome/6.0.416.0 Safari/534.1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.1\
     (KHTML, like Gecko) Chrome/6.0.414.0 Safari/534.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.9 (KHTML, \
     like Gecko) Chrome/6.0.400.0 Safari/533.9",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.8 (KHTML, \
     like Gecko) Chrome/6.0.397.0 Safari/533.8",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.2 (KHTML, \
     like Gecko) Chrome/6.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, \
     like Gecko) Chrome/5.0.375.999 Safari/533.4",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.4 (KHTML, like G\
     ecko) Chrome/5.0.375.99 Safari/533.4",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.4 (KHTML, \
     like Gecko) Chrome/5.0.375.99 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.99 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.99 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.99 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/533.4 (KHTML\
     , like Gecko) Chrome/5.0.375.86 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.70 Safari/533.4",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.4 (KHTML, \
     like Gecko) Chrome/5.0.375.127 Safari/533.4",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.4 (KHTML, \
     like Gecko) Chrome/5.0.375.126 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; fr-FR) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.126 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.375.125 Safari/533.4",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.4 (KHTML, \
     like Gecko) Chrome/5.0.370.0 Safari/533.4",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.4 (KHTML, like G\
     ecko) Chrome/5.0.368.0 Safari/533.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/533.4 (KHTML, like Gec\
     ko) Chrome/5.0.366.2 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.366.0 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/533.4\
     (KHTML, like Gecko) Chrome/5.0.366.0 Safari/533.4",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-US) AppleWebKit/533.3\
     (KHTML, like Gecko) Chrome/5.0.363.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; OpenBSD i386; en-US) AppleWebKit/533.3 (KHTML, like G\
     ecko) Chrome/5.0.359.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; x86_64 Linux; en_GB, en_US) AppleWebKit/533.3 (KHTML,\
     like Gecko) Chrome/5.0.358.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.3 (KHTML, like G\
     ecko) Chrome/5.0.358.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/533.3 (KHTML, like Gec\
     ko) Chrome/5.0.358.0 Safari/533.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/5.0.357.0 Safari/533.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/5.0.356.0 Safari/533.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/5.0.355.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.3 (KHTML, like G\
     ecko) Chrome/5.0.354.0 Safari/533.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/5.0.354.0 Safari/533.3",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.3 (KHTML, like G\
     ecko) Chrome/5.0.353.0 Safari/533.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.3 (KHTML, \
     like Gecko) Chrome/5.0.353.0 Safari/533.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/533.2\
     (KHTML, like Gecko) Chrome/5.0.343.0 Safari/533.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/533.2\
     (KHTML, like Gecko) Chrome/5.0.343.0 Safari/533.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_7_0; en-US) AppleWebKit/533.2\
     (KHTML, like Gecko) Chrome/5.0.342.7 Safari/533.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/533.2\
     (KHTML, like Gecko) Chrome/5.0.342.7 Safari/533.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.2 (KHTML, \
     like Gecko) Chrome/5.0.342.5 Safari/533.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.2 (KHTML, like G\
     ecko) Chrome/5.0.342.3 Safari/533.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.2 (KHTML, \
     like Gecko) Chrome/5.0.342.3 Safari/533.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/533.2 (KHTML, \
     like Gecko) Chrome/5.0.342.2 Safari/533.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.2 (KHTML, like G\
     ecko) Chrome/5.0.342.1 Safari/533.2",
	"Mozilla/5.0 (X11; U; Linux i586; en-US) AppleWebKit/533.2 (KHTML, like Gec\
     ko) Chrome/5.0.342.1 Safari/533.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.2 (KHTML, \
     like Gecko) Chrome/5.0.342.1 Safari/533.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/533.1 (KHTML, like G\
     ecko) Chrome/5.0.335.0 Safari/533.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN) AppleWebKit/533.16 (KHTML,\
     like Gecko) Chrome/5.0.335.0 Safari/533.16",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.9 (KHTML, \
     like Gecko) Chrome/5.0.310.0 Safari/532.9",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.9 (KHTML, like G\
     ecko) Chrome/5.0.309.0 Safari/532.9",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.9 (KHTML, like G\
     ecko) Chrome/5.0.308.0 Safari/532.9",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.9\
     (KHTML, like Gecko) Chrome/5.0.307.11 Safari/532.9",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.9 (KHTML, \
     like Gecko) Chrome/5.0.307.1 Safari/532.9",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, \
     like Gecko) Chrome/4.1.249.1025 Safari/532.5",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.8\
     (KHTML, like Gecko) Chrome/4.0.302.2 Safari/532.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.8 (KHTML, \
     like Gecko) Chrome/4.0.288.1 Safari/532.8",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.8 (KHTML, like Gec\
     ko) Chrome/4.0.277.0 Safari/532.8",
	"Mozilla/5.0 (X11; U; Slackware Linux x86_64; en-US) AppleWebKit/532.5 (KHT\
     ML, like Gecko) Chrome/4.0.249.30 Safari/532.5",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; it-IT) AppleWebKit/532.5 (KHTML, \
     like Gecko) Chrome/4.0.249.25 Safari/532.5",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, \
     like Gecko) Chrome/4.0.249.0 Safari/532.5",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_8; en-US) AppleWebKit/532.5 (\
     KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.5 (KHTML, \
     like Gecko) Chrome/4.0.246.0 Safari/532.5",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.4 (KHTML, \
     like Gecko) Chrome/4.0.241.0 Safari/532.4",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.4 (KHTML, like Gec\
     ko) Chrome/4.0.237.0 Safari/532.4 Debian",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.3 (KHTML, \
     like Gecko) Chrome/4.0.227.0 Safari/532.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.3 (KHTML, \
     like Gecko) Chrome/4.0.224.2 Safari/532.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.3 (KHTML, \
     like Gecko) Chrome/4.0.223.5 Safari/532.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.4 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.3 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; de-DE) Chrome/4.0.223.3 Safari/53\
     2.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.223.2 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.223.2 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.2 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.2 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.223.1 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.1 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.1 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.223.0 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.8 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.7 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.222.6 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.6 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.6 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.222.5 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.5 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.5 Safari/532.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.2\
     (KHTML, like Gecko) Chrome/4.0.222.5 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.222.4 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.4 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.4 Safari/532.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.2\
     (KHTML, like Gecko) Chrome/4.0.222.4 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.3 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.3 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.3 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.222.2 Safari/532.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.2\
     (KHTML, like Gecko) Chrome/4.0.222.2 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.12 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.12 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.12 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.222.1 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.222.0 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.221.8 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.2 (KHTML,\
     like Gecko) Chrome/4.0.221.8 Safari/532.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.2\
     (KHTML, like Gecko) Chrome/4.0.221.8 Safari/532.2",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.2\
     (KHTML, like Gecko) Chrome/4.0.221.8 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.221.7 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.221.6 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.221.6 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, \
     like Gecko) Chrome/4.0.221.6 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.2 (KHTML, like G\
     ecko) Chrome/4.0.221.3 Safari/532.2",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.2 (KHTML, like Gec\
     ko) Chrome/4.0.221.0 Safari/532.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.220.1 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.6 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.5 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.5 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.4 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.1 (KHTML, like G\
     ecko) Chrome/4.0.219.3 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.3 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.3 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.219.0 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.1 (KHTML, like G\
     ecko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.1 (KHTML, like Gec\
     ko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.1 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.1 (KHTML, like G\
     ecko) Chrome/4.0.213.0 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.1 (KHTML, like Gec\
     ko) Chrome/4.0.213.0 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.0 Safari/532.1",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.1 (KHTML, \
     like Gecko) Chrome/4.0.213.0 Safari/532.1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.1\
     (KHTML, like Gecko) Chrome/4.0.212.1 Safari/532.1",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-US) AppleWebKit/532.1\
     (KHTML, like Gecko) Chrome/4.0.212.1 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like G\
     ecko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.1 (KHTML, like Gec\
     ko) Chrome/4.0.212.0 Safari/532.1",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Gec\
     ko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.0\
     (KHTML, like Gecko) Chrome/4.0.212.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.7 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.7 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.4 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.4 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.4 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like G\
     ecko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Gec\
     ko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.211.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.211.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.211.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.211.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.211.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.210.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.210.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.209.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.209.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.209.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.209.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.208.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.208.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.208.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.208.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.208.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; FreeBSD i386; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.207.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.206.1 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.206.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.206.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.206.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.205.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.204.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.204.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.204.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.204.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.204.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.203.4 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.203.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.202.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML\
     , like Gecko) Chrome/4.0.202.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0 (x86_64); de-DE) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.202.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; de-DE) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.202.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML\
     , like Gecko) Chrome/4.0.202.0 Safari/525.13.",
    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_0; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/4.0.202.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.201.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.201.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/4.0.201.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.201.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/3.0.198.1 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML\
     , like Gecko) Chrome/3.0.198.1 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/3.0.198.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML\
     , like Gecko) Chrome/3.0.198.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.198.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.198.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.198 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/3.0.198 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/3.0.198 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/3.0.197.11 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.197.11 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.197.11 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.197.11 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/3.0.197.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML\
     , like Gecko) Chrome/3.0.197.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.197.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/3.0.197 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.196.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.196.2 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.196.2 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Ge\
     cko) Chrome/3.0.196.0 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML\
     , like Gecko) Chrome/3.0.196.0 Safari/532.0",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-US) AppleWebKit/532.\
     0 (KHTML, like Gecko) Chrome/3.0.196 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.6 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.6 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.6 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.6 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.6 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.4 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.33 Safari/532.0",
	"Mozilla/4.0 (Windows; U; Windows NT 5.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.33 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.3 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.3 Safari/532.0",
	"Mozilla/6.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.27 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/532.0 (KHTML, like \
     Gecko) Chrome/3.0.195.24 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.24 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.21 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.21 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.21 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML,\
     like Gecko) Chrome/3.0.195.21 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.20 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.20 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.17 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.17 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.10 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.10 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.10 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/532.0 (KHTML, like Gec\
     ko) Chrome/3.0.195.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.1 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, \
     like Gecko) Chrome/3.0.195.1 Safari/532.0",
	"Mozilla/5.0 (X11; U; Linux i686; en-US) AppleWebKit/531.4 (KHTML, like Gec\
     ko) Chrome/3.0.194.0 Safari/531.4",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.4 (KHTML, \
     like Gecko) Chrome/3.0.194.0 Safari/531.4",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.3 (KHTML, \
     like Gecko) Chrome/3.0.193.2 Safari/531.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/531.3 (KHTML, \
     like Gecko) Chrome/3.0.193.2 Safari/531.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/531.3 (KHTML, \
     like Gecko) Chrome/3.0.193.2 Safari/531.3",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/531.3 (KHTML, \
     like Gecko) Chrome/3.0.193.0 Safari/531.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-US) AppleWebKit/531.3\
     (KHTML, like Gecko) Chrome/3.0.192 Safari/531.3",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.2 (KHTML, \
     like Gecko) Chrome/3.0.191.3 Safari/531.2",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.0 (KHTML, \
     like Gecko) Chrome/3.0.191.0 Safari/531.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.0 (KHTML, \
     like Gecko) Chrome/3.0.191.0 Safari/531.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.0 (KHTML, \
     like Gecko) Chrome/2.0.182.0 Safari/532.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/531.0 (KHTML, \
     like Gecko) Chrome/2.0.182.0 Safari/531.0",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/530.0 (KHTML, \
     like Gecko) Chrome/2.0.182.0 Safari/531.0",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.8 (KHTML, \
     like Gecko) Chrome/2.0.178.0 Safari/530.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.8 (KHTML, \
     like Gecko) Chrome/2.0.177.1 Safari/530.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.8 (KHTML, \
     like Gecko) Chrome/2.0.177.0 Safari/530.8",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.7 (KHTML, \
     like Gecko) Chrome/2.0.177.0 Safari/530.7",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/530.7 (KHTML, \
     like Gecko) Chrome/2.0.176.0 Safari/530.7",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.7 (KHTML,\
     like Gecko) Chrome/2.0.176.0 Safari/530.7",
	"Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/530.7 (KHTML\
     , like Gecko) Chrome/2.0.175.0 Safari/530.7",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.7 (KHTML,\
     like Gecko) Chrome/2.0.175.0 Safari/530.7",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.6 (KHTML,\
     like Gecko) Chrome/2.0.175.0 Safari/530.6",
	"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/530.6 (KHTML,\
     like Gecko) Chrome/2.0.174.0 Safari/530.6",
	"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/530.6 (KHTML, \
     like Gecko) Chrome/2.0.174.0 Safari/530.6",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.6 (KHTML, \
     like Gecko) Chrome/2.0.174.0 Safari/530.6",
	"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML,\
     like Gecko) Chrome/2.0.174.0 Safari/530.5",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-US) AppleWebKit/530.6\
    (KHTML, like Gecko) Chrome/2.0.174.0 Safari/530.6",
];

fn get_random_ua() -> &'static str {
    let mut rng = rand::thread_rng();
    let indx = rng.gen_range(0..USER_AGENTS.len());
    return USER_AGENTS[indx];
}

pub fn do_request(url: &Url) -> Result<String, reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let mut request_headers = HeaderMap::new();
    request_headers
        .insert(USER_AGENT, HeaderValue::from_static(get_random_ua()));

    Ok(client
        .get(url.as_ref())
        .timeout(Duration::from_secs(5))
        .headers(request_headers)
        .send()?
        .text()?)
}

pub fn get_urls(base_url: &Url, doc: &str) -> HashSet<Url> {
    let base_parser = Url::options().base_url(Some(&base_url));
    let v: HashSet<Url> = Document::from(doc)
        .find(Name("a"))
        .filter_map(|n| n.attr("href"))
        .filter_map(|link| base_parser.parse(link).ok())
        .filter(|url| {
            !url.path().ends_with(".css") || !url.path().ends_with(".js")
        })
        .collect();

    return v;
}
