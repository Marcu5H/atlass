use log::{error, info};

/// Include configuration crate
use config;

mod crawl;
mod data;
mod scrape;

fn main() {
    // Initialise the logger, edit `LOG_LEVEL` in the makefile if using it
    env_logger::init();

    info!("Fetching configuration");
    // Load the configuration file
    let config = config::get_config("Atlas.toml");

    info!("domains_path: {}", config.engine.domains_path);
    info!("depth: {}", config.engine.depth);

    let mut crawler = crawl::Crawler::new(&config.engine);
    if let Some(err) = crawler.start_crawling() {
        error!("Error occured while crawling: {}", err);
    }
}
