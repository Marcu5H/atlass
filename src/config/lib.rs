use std::fs;

use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub engine: Engine,
    pub search: Search,
}

#[derive(Deserialize)]
pub struct Engine {
    pub data_path: String,
    pub domains_path: String,
    pub depth: u16,
    pub tokenizer: String,
}

#[derive(Deserialize)]
pub struct Search {
    pub gpc_search_key: String,
    pub gpc_id: String,
}

pub fn get_config(config_file_path: &str) -> Config {
    let config_string = fs::read_to_string(config_file_path);
    if config_string.is_err() {
        return Config {
            engine: Engine {
                data_path: "data/".to_string(),
                domains_path: "data/domains.txt".to_string(),
                depth: 3,
                tokenizer: "NO".to_string(),
            },
            search: Search {
                gpc_search_key: "NO".to_string(),
                gpc_id: "NO".to_string(),
            },
        };
    }

    let config = toml::from_str(&config_string.unwrap());
    if config.is_err() {
        return Config {
            engine: Engine {
                data_path: "data/".to_string(),
                domains_path: "data/domains.txt".to_string(),
                depth: 3,
                tokenizer: "NO".to_string(),
            },
            search: Search {
                gpc_search_key: "NO".to_string(),
                gpc_id: "NO".to_string(),
            },
        };
    }

    config.unwrap()
}
