use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use rocket::catchers;
use rocket::fs::{relative, FileServer};
use rocket_dyn_templates::Template;

use config::get_config;

use stem::Stem;

//mod google_search;
mod hbs;
mod local_search;

#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    println!("Loading config...");
    let config = get_config("Atlas.toml");
    //let creds = google_search::GoogleCreds {
    //    gpc_key: config.search.gpc_search_key,
    //    gpc_id: config.search.gpc_id,
    //};

    println!("Loading searcher...");
    let tokenizer_path = PathBuf::from(config.engine.tokenizer);
    let corpus_path = PathBuf::from(config.engine.data_path);
    let mut searcher = local_search::Searcher::new(tokenizer_path, corpus_path);

    println!("Loading corpus");
    let corpus_size = searcher.init_corpus();
    if corpus_size < 1 {
        panic!("Corpus is empty");
    }

    let searcher_mutex = Arc::new(Mutex::new(searcher));

    let stemmer = Stem::new();
    let stemmer_mutex = Arc::new(Mutex::new(stemmer));

    println!("Starting server...");
    let _rocket = rocket::build()
        .manage(searcher_mutex)
        .manage(stemmer_mutex)
        //.manage(creds)
        //.mount("/s", rocket::routes![google_search::query])
        .mount("/s", rocket::routes![local_search::query])
        .mount("/", FileServer::from(relative!("static")))
        .register("/", catchers![hbs::four_o_four])
        .attach(Template::fairing())
        .launch()
        .await?;

    Ok(())
}
