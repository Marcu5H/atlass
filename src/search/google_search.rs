use std::time::Instant;

use reqwest::Client;
use serde::{Deserialize, Serialize};

use rocket::State;

use rocket_dyn_templates::{context, Template};

pub struct GoogleCreds {
    pub gpc_key: String,
    pub gpc_id: String,
}

#[derive(Serialize, Deserialize)]
struct Item {
    title: String,
    url: String,
    html_url: String,
    snippet: String,
}

#[rocket::get("/?<q>")]
pub async fn query(q: &str, creds: &State<GoogleCreds>) -> Template {
    let search_url = format!(
        "https://www.googleapis.com/customsearch/v1?key={}&cx={}&q={}",
        creds.gpc_key, creds.gpc_id, q
    );

    let start_time = Instant::now();

    let client = Client::new();
    let json_result: serde_json::Value =
        match client.get(search_url).send().await {
            Ok(r) => match r.json::<serde_json::Value>().await {
                Ok(d) => d,
                Err(_) => {
                    return Template::render(
                        "no_results",
                        context! {
                            time: format!("{:?}",
                                          start_time.elapsed())
                        },
                    )
                }
            },
            Err(_) => {
                return Template::render(
                    "no_results",
                    context! {
                        time: format!("{:?}",
                                        start_time.elapsed())
                    },
                )
            }
        };

    let json_items = json_result["items"].as_array();
    if json_items.is_none() {
        return Template::render("error/404", context! {uri: "Not found"});
    }

    let mut items: Vec<Item> = Vec::new();
    if let Some(its) = json_items {
        for item in its.iter() {
            if let Some(item_obj) = item.as_object() {
                items.push(Item {
                    title: item_obj
                        .get("title")
                        .unwrap_or(&serde_json::Value::Null)
                        .as_str()
                        .unwrap_or("null")
                        .to_string(),
                    url: item_obj
                        .get("link")
                        .unwrap_or(&serde_json::Value::Null)
                        .as_str()
                        .unwrap_or("null")
                        .to_string(),
                    html_url: item_obj
                        .get("displayLink")
                        .unwrap_or(&serde_json::Value::Null)
                        .as_str()
                        .unwrap_or("null")
                        .to_string(),
                    snippet: item_obj
                        .get("snippet")
                        .unwrap_or(&serde_json::Value::Null)
                        .as_str()
                        .unwrap_or("null")
                        .to_string(),
                })
            }
        }
    }

    return Template::render(
        "result",
        context! {
            results: items.len(),
            time: format!("{:?}", start_time.elapsed()),
            items
        },
    );
}
