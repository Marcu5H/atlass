use std::sync::{Arc, Mutex};
use std::time::Instant;

use std::collections::HashMap;
use std::f32::consts::LN_2;

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;

use url::Url;

use ordered_float::OrderedFloat;

use tokenizers::tokenizer::Tokenizer;

use serde::{Deserialize, Serialize};

use rocket::State;

use stem::Stem;

use rocket_dyn_templates::{context, Template};

#[derive(Serialize, Deserialize)]
struct Item {
    title: String,
    url: String,
    html_url: String,
    score: f32,
}

pub struct SearchResult {
    score: f32,
    path: PathBuf,
}

struct CorpusEntry {
    path: PathBuf,
    ids: Vec<u32>,
}

pub struct Searcher {
    tokenizer: Tokenizer,
    corpus_path: PathBuf,
    corpus: Vec<CorpusEntry>,
    pub corpus_size: u32,
}

impl Searcher {
    pub fn new(tokenizer_path: PathBuf, corpus_path: PathBuf) -> Self {
        let tokenizer = Tokenizer::from_file(tokenizer_path).unwrap();
        return Self {
            tokenizer,
            corpus_path,
            corpus: Vec::new(),
            corpus_size: 0,
        };
    }

    pub fn init_corpus(&mut self) -> u32 {
        let mut corpus_size = 0;
        for entry in match std::fs::read_dir(&self.corpus_path) {
            Ok(e) => e,
            Err(_) => return corpus_size,
        } {
            let entry_dir = match entry {
                Ok(e) => e,
                Err(_) => continue,
            };

            for page in match std::fs::read_dir(entry_dir.path()) {
                Ok(e) => e,
                Err(_) => return corpus_size,
            } {
                let page_path = match page {
                    Ok(p) => p,
                    Err(_) => continue,
                };

                let mut path_to_tokenized_doc = PathBuf::from(page_path.path());
                path_to_tokenized_doc.push("tokenized_doc.json");
                if !path_to_tokenized_doc.exists() {
                    continue;
                }

                let doc_file = match File::open(path_to_tokenized_doc) {
                    Ok(f) => f,
                    Err(_) => continue,
                };
                let doc_reader = BufReader::new(doc_file);
                let doc_token_ids: Vec<u32> =
                    match serde_json::from_reader(doc_reader) {
                        Ok(t) => t,
                        Err(_) => continue,
                    };

                self.corpus.push(CorpusEntry {
                    path: page_path.path(),
                    ids: doc_token_ids,
                });
                corpus_size += 1;
            }
        }

        self.corpus_size = corpus_size;
        return corpus_size;
    }

    fn idf(&self, query: &Vec<u32>) -> HashMap<u32, f32> {
        let mut count: HashMap<u32, u32> = HashMap::new();

        for doc in &self.corpus {
            for &term in query {
                if doc.ids.contains(&&term) {
                    *count.entry(term).or_insert(0) += 1;
                }
            }
        }

        let mut _idf: HashMap<u32, f32> = HashMap::new();

        let corpus_size = self.corpus.len() as u32;
        for entry in count {
            _idf.insert(
                entry.0,
                (corpus_size as f32 / entry.1 as f32).ln() / LN_2,
            );
        }

        return _idf;
    }

    fn tf_idf(
        &self,
        query: &Vec<u32>,
        doc: &Vec<u32>,
        idf: &HashMap<u32, f32>,
    ) -> HashMap<u32, f32> {
        let mut tf: HashMap<u32, f32> = HashMap::new();
        for &token_id in query {
            let token_count = doc.iter().filter(|&&id| id == token_id).count();
            let mut term_idf: f32 = 1.0;
            if let Some((_, freq)) = idf.get_key_value(&token_id) {
                term_idf = *freq;
            }
            tf.insert(
                token_id,
                ((token_count as f32) / (doc.len() as f32)) * term_idf,
            );
        }

        return tf;
    }

    pub fn search(&self, query: &str) -> Vec<SearchResult> {
        let mut results: Vec<SearchResult> = Vec::new();
        if self.corpus.is_empty() {
            return results;
        }

        let query: Vec<u32> = match self.tokenizer.encode(query, false) {
            Ok(e) => e.get_ids().to_vec(),
            Err(_) => return results,
        };

        let _idf = self.idf(&query);

        for entry in &self.corpus {
            let tf_idf_for_doc = self.tf_idf(&query, &entry.ids, &_idf);

            let mut doc_score: f32 = 0.0;
            for _tf_idf in tf_idf_for_doc.values() {
                doc_score += _tf_idf;
            }

            results.push(SearchResult {
                score: doc_score,
                path: PathBuf::from(&entry.path),
            });
        }

        return results;
    }
}

#[rocket::get("/?<q>")]
pub async fn query(
    q: &str,
    m_searcher: &State<Arc<Mutex<Searcher>>>,
    m_stemmer: &State<Arc<Mutex<Stem>>>,
) -> Template {
    let mut searcher = m_searcher.lock().unwrap();
    let stemmer = m_stemmer.lock().unwrap();

    let start_time = Instant::now();

    let stemmed_query = stemmer.stem(&q.to_lowercase());

    let mut search_results = searcher.search(&stemmed_query);
    let mut items: Vec<Item> = Vec::new();
    search_results.sort_by_key(|r| OrderedFloat(r.score));
    search_results.reverse();
    for (index, res) in search_results.iter().enumerate() {
        if res.score.is_nan() {
            continue;
        } else if res.score == 0.0 || index >= 100 {
            break;
        }

        let mut path_to_url = PathBuf::from(&res.path);
        path_to_url.push("url");
        if !path_to_url.exists() {
            continue;
        }

        let url_file = match File::open(path_to_url) {
            Ok(f) => f,
            Err(_) => continue,
        };

        let mut reader = BufReader::new(url_file);
        let mut url = String::new();
        if let Err(_) = reader.read_line(&mut url) {
            continue;
        }

        let real_url = match Url::parse(&url) {
            Ok(url) => url,
            Err(_) => continue,
        };

        items.push(Item {
            title:  real_url.path().to_string(),
            url: real_url.as_str().to_string(),
            html_url: real_url.host_str().unwrap_or("Failed to parse")
                                         .to_string(),
            score: res.score,
        });
    }

    return Template::render(
        "result",
        context! {
            query: q,
            results: items.len(),
            time: format!("{:?}", start_time.elapsed()),
            corpus_size: searcher.corpus_size,
            items,
        },
    );
}
