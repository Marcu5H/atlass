use rocket::Request;

use rocket_dyn_templates::{context, Template};

#[rocket::catch(404)]
pub fn four_o_four(req: &Request<'_>) -> Template {
    Template::render(
        "error/404",
        context! {
            uri: req.uri()
        },
    )
}
