use rust_stemmers::{Algorithm, Stemmer};

pub struct Stem {
    stemmer: Stemmer,
}

impl Stem {
    pub fn new() -> Self {
        return Self {
            stemmer: Stemmer::create(Algorithm::Norwegian),
        };
    }

    pub fn stem(&self, doc: &str) -> String {
        let mut stemmed = String::new();
        for word in doc.split_whitespace() {
            stemmed.push_str(&format!("{} ", self.stemmer.stem(word)));
        }

        return stemmed;
    }
}
