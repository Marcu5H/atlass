LOG_LEVEL = info

export RUST_LOG := $(LOG_LEVEL)
engine:
	cargo run --release --bin engine
