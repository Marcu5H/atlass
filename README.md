![Logo](images/logo.png)
##### Logo generated using DALL-E

# Atlas Search
Atlas Search is a standalone search engine and web crawler.

## Features
 * Single threaded queue based web crawler
 * No database needed (atlass is only using filesystem)
 * Search in documents using TF-IDF
 * Mostly written in Rust for good performance and safety
 * HTML/CSS web UI

## Installing
TODO

## Running
TODO

## UI Preview
TODO